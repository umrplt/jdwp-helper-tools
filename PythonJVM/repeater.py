import _thread
import socket
import struct
import binascii

DEBUGGER_PORT = 5005
JVM_PORT = 5006

def forward(source, destination, printing):
    string = ' '
    while string:
        string = source.recv(1024)
        if printing:
            if string and len(string) > 8:
                length, id = struct.unpack('>ii', string[0:8])
                data = string[8:]
                print(binascii.hexlify(data))
        if string:
            destination.sendall(string)

dock_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dock_socket.bind(('127.0.0.1', DEBUGGER_PORT))
dock_socket.listen(1)
while True:
    client_socket = dock_socket.accept()[0]
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.connect(('127.0.0.1', JVM_PORT))
    _thread.start_new_thread(forward, (server_socket, client_socket, True))
    _thread.start_new_thread(forward, (client_socket, server_socket, False))