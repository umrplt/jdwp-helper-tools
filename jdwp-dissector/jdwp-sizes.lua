JDWP_IDSIZES = { }

local instance

function JDWP_IDSIZES.get_instance()
    if not instance then
        instance = JDWP_IDSIZES
        instance["byte"] = 1
        instance["boolean"] = 1
        instance["int"] = 4
        instance["long"] = 8

        instance["char"] = 2
        instance["float"] = 4
        instance["double"] = 8

        instance["objectID"] = 0
        instance["tagged-objectID"] = 0
        instance["threadID"] = 0
        instance["threadGroupID"] = 0
        instance["stringID"] = 0
        instance["classLoaderID"] = 0
        instance["classObjectID"] = 0
        instance["arrayID"] = 0

        instance["referenceTypeID"] = 0
        instance["classID"] = 0
        instance["interfaceID"] = 0
        instance["arrayTypeID"] = 0

        instance["methodID"] = 0
        instance["fieldID"] = 0
        instance["frameID"] = 0
        instance["location"] = 0
    end

    return instance
end

function JDWP_IDSIZES.set_objectID(size)
    instance["objectID"] = size
    instance["tagged-objectID"] = size + 1
    instance["threadID"] = size
    instance["threadGroupID"] = size
    instance["stringID"] = size
    instance["classLoaderID"] = size
    instance["classObjectID"] = size
    instance["arrayID"] = size
end

function JDWP_IDSIZES.set_referenceTypeID(size)
    instance["referenceTypeID"] = size
    instance["classID"] = size
    instance["interfaceID"] = size
    instance["arrayTypeID"] = size
end

function JDWP_IDSIZES.set_methodID(size)
    JDWP_IDSIZES.get_instance()["methodID"] = size
end

function JDWP_IDSIZES.set_fieldID(size)
    JDWP_IDSIZES.get_instance()["fieldID"] = size
end

function JDWP_IDSIZES.set_frameID(size)
    JDWP_IDSIZES.get_instance()["frameID"] = size
end

function JDWP_IDSIZES.set_location(loc)
    JDWP_IDSIZES.get_instance()["location"] = loc
end