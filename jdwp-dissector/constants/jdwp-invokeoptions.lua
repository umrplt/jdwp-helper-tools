-- JDWP InvokeOptions Constants
-- The invoke options are a combination of zero or more of the following bit flags:
local INVOKE_SINGLE_THREADED = 0x01 -- otherwise, all threads started.
local INVOKE_NONVIRTUAL      = 0x02 -- otherwise, normal virtual invoke (instance methods only)

local invokeoptions = {
    [INVOKE_SINGLE_THREADED] = "Invoke single-threaded",
    [INVOKE_NONVIRTUAL] = "Invoke non-virtual"
}

invokeoptions[INVOKE_SINGLE_THREADED + INVOKE_NONVIRTUAL] = invokeoptions[INVOKE_SINGLE_THREADED] .. ", " .. invokeoptions[INVOKE_NONVIRTUAL]

return invokeoptions