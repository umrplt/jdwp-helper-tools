-- JDWP EventKind Constants
SINGLE_STEP	                  = 1	 
BREAKPOINT	                  = 2	 
FRAME_POP	                  = 3	 
EXCEPTION	                  = 4	 
USER_DEFINED	              = 5	 
THREAD_START	              = 6	 
THREAD_DEATH	              = 7	 
--THREAD_END	                  = 7	-- obsolete - was used in jvmdi  
CLASS_PREPARE	              = 8	 
CLASS_UNLOAD	              = 9	 
CLASS_LOAD	                  = 10	 
FIELD_ACCESS	              = 20	 
FIELD_MODIFICATION	          = 21	 
EXCEPTION_CATCH	              = 30	 
METHOD_ENTRY	              = 40	 
METHOD_EXIT	                  = 41	 
METHOD_EXIT_WITH_RETURN_VALUE = 42	 
MONITOR_CONTENDED_ENTER	      = 43	 
MONITOR_CONTENDED_ENTERED	  = 44	 
MONITOR_WAIT	              = 45	 
MONITOR_WAITED	              = 46	 
VM_START	                  = 90	 
--VM_INIT	                      = 90	-- obsolete - was used in jvmdi  
VM_DEATH	                  = 99	 
VM_DISCONNECTED	              = 100	-- Never sent across JDWP

local eventkinds = {
    [SINGLE_STEP] = "Single step",
    [BREAKPOINT] = "Breakpoint",
    [FRAME_POP] = "Frame pop",
    [EXCEPTION] = "Exception",
    [USER_DEFINED] = "User defined",
    [THREAD_START] = "Thread start",
    [THREAD_DEATH] = "Thread death",
--  [THREAD_END] = " Thread end",
    [CLASS_PREPARE] = "Class prepare",
    [CLASS_UNLOAD] = "Class unload",
    [CLASS_LOAD] = "Class load",
    [FIELD_ACCESS] = "Field access",
    [FIELD_MODIFICATION] = "Field modification",
    [EXCEPTION_CATCH] = "Exception catch",
    [METHOD_ENTRY] = "MEthod entry",
    [METHOD_EXIT] = "Method exit",
    [METHOD_EXIT_WITH_RETURN_VALUE] = "Method exit with return value",
    [MONITOR_CONTENDED_ENTER] = "Monitor contended enter",
    [MONITOR_CONTENDED_ENTERED] = "Monitor contendend entered",
    [MONITOR_WAIT] = "Monitor wait",
    [MONITOR_WAITED] = "Monitor waited",
    [VM_START] = "VM start",
--  [VM_INIT] = "VM init",
    [VM_DEATH] = "VM death",
    [VM_DISCONNECTED] = "VM disconnected",
}

return eventkinds