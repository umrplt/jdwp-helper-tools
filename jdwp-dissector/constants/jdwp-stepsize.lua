-- StepSize Constants
local MIN	 = 0 -- Step by the minimum possible amount (often a bytecode instruction).  
local LINE = 1 -- Step to the next source line unless there is no line number information in which case a MIN step is done instead.

local stepsizes = {
    [MIN] = "min",
    [LINE] = "line"
}

return stepsizes