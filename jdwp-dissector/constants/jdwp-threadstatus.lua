--JDWP ThreadStatus Constants
local ZOMBIE	 = 0	 
local RUNNING	 = 1	 
local SLEEPING   = 2	 
local MONITOR	 = 3	 
local WAIT	     = 4	 

local threadstates =
{
    [ZOMBIE] = "Zombie",
    [RUNNING] = "Running",
    [SLEEPING] = "Sleeping",
    [MONITOR] = "Monitor",
    [WAIT] = "Wait"
}

return threadstates