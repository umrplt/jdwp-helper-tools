-- JDWP TypeTag Constants
local CLASS	    = 1 -- ReferenceType is a class.  
local INTERFACE = 2 -- ReferenceType is an interface.  
local ARRAY	    = 3 -- ReferenceType is an array.

local typetags = {
    [CLASS]	    = "Class", 
    [INTERFACE] = "Interface",
    [ARRAY]	    = "Array"
}

return typetags