-- JDWP SuspendStatus Constants
local SUSPEND_STATUS_SUSPENDED = 0x1

local suspendstates = {
    [SUSPEND_STATUS_SUSPENDED] = "Suspended"
}

return suspendstates