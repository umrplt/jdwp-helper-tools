-- SuspendPolicy Constants
local NONE	       = 0 --	Suspend no threads when this event is encountered.  
local EVENT_THREAD = 1 --	Suspend the event thread when this event is encountered.  
local ALL	       = 2 -- Suspend all threads when this event is encountered.

local suspendpolicies = {
    [NONE] = "none",
    [EVENT_THREAD] = "eventh thread",
    [ALL] = "all"
}

return suspendpolicies