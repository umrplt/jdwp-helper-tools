-- JDWP ClassStatus Constants
local VERIFIED	  = 1	 
local PREPARED	  = 2
local INITIALIZED = 4	 
local ERROR	      = 8

local classstates = 
{
    [VERIFIED]	  = "Verified",	 
    [PREPARED]	  = "Prepared",
    [INITIALIZED] = "Initialized",
    [ERROR]	      = "Error"
}

classstates[VERIFIED + PREPARED] = classstates[VERIFIED] .. ", " .. classstates[PREPARED]
classstates[VERIFIED + INITIALIZED] =  classstates[VERIFIED] .. ", " .. classstates[INITIALIZED]
classstates[PREPARED + INITIALIZED] = classstates[PREPARED] .. ", " .. classstates[INITIALIZED]
classstates[VERIFIED + PREPARED + INITIALIZED] = classstates[VERIFIED] .. ", " .. classstates[PREPARED] .. ", " .. classstates[INITIALIZED]

return classstates