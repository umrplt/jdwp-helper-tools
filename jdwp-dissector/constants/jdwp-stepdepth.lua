-- JDWP StepDepth Constants
local INTO = 0 -- Step into any method calls that occur before the end of the step.  
local OVER = 1 -- Step over any method calls that occur before the end of the step.  
local OUT  = 2 -- Step out of the current method.  

local stepdepths = {
    [INTO] = "into",
    [OVER] = "over",
    [OUT] = "out"
}

return stepdepths