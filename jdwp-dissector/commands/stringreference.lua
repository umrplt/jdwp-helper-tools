package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Value Command (1)
-- Request
function dissect_value_request(buffer)
    local stringObject = 0
    
    stringObject, buffer = extract_objectID(buffer)

    return {
        ["label"] = "Value Request",
        ["String Object ID"] = stringObject
    }
end
-- Reply
function dissect_value_reply(buffer)
    local stringValue = ""

    stringValue, buffer = extract_string(buffer)

    return {
        ["label"] = "Value Reply",
        ["String Value"] = stringValue
    }
end