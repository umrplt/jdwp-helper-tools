package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- ReferenceType Command (1)
-- Request
function dissect_referencetype_request(buffer)
    local object = 0

    object, buffer = extract_objectID(buffer)

    return {
        ["label"] = "ReferenceType Request",
        ["Object ID"] = object
    }
end
-- Reply
function dissect_referencetype_reply(buffer)
    local refTypeTag = 0
    local typeID = 0

    refTypeTag, buffer = extract_byte(buffer)
    typeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ReferenceType Reply",
        ["Reference Type"] = typetags[refTypeTag],
        ["Type ID"] = typeID
    }
end

-- GetValues Command (2)
-- Request
function dissect_getobjectvalues_request(buffer)
    local object = 0
    local fields = 0
    local fieldinfo = {}

    object, buffer = extract_objectID(buffer)
    fields, buffer = extract_int(buffer)
    fieldinfo["label"] = "Fields (" .. fields .. ")"
    for i=1, fields do
        local fieldID = 0

        fieldID, buffer = extract_fieldID(buffer)

        fieldinfo[i] = {
            ["Field ID"] = fieldID
        }
    end

    return {
        ["label"] = "GetValues Request",
        ["Object ID"] = object,
        ["Fields"] = fieldinfo 
    }
end
-- Reply
function dissect_getobjectvalues_reply(buffer)
    local values = 0 
    local valueinfo = {}

    values, buffer = extract_int(buffer)
    valueinfo["label"] = "Values (" .. values .. ")"
    for i=1, values do
        local value = 0

        value, buffer = extract_value(buffer)

        valueinfo[i] = {
            ["Value"] = value
        }
    end

    return {
        ["label"] = "GetValues Request",
        ["Values"] = valueinfo
    }
end

-- SetValues Command (3)
-- Request
function dissect_setobjectvalues_request(buffer)
    local object = 0
    local values = 0
    local valueinfo = {}

    object, buffer = extract_objectID(buffer)
    values, buffer = extract_int(buffer)
    valueinfo["label"] = "Values (" .. values .. ")"
    for i=1, values do
        local fieldID = 0
        local value = 0

        fieldID, buffer = extract_fieldID(buffer)
        value, buffer = extract_untagged_value(buffer)

        valueinfo[i] = {
            ["Field ID"] = fieldID,
            ["Value"] = value
        }
    end

    return {
        ["label"] = "SetValues Request",
        ["Object ID"] = object,
        ["Values"] = valueinfo
    }
end
-- No Reply

-- MonitorInfo Command (5)
-- Request
function dissect_objectmonitorinfo_request(buffer)
    local object = 0

    object, buffer = extract_objectID(buffer)

    return {
        ["label"] = "MonitorInfo Request",
        ["Object ID"] = object
    }
end
-- Reply
function dissect_objectmonitorinfo_reply(buffer)
    local owner = 0
    local entryCount = 0
    local waiters = 0
    local waiterinfo = {}

    owner, buffer = extract_threadID(buffer)
    entryCount, buffer = extract_int(buffer)
    waiters, buffer = extract_int(buffer)
    waiterinfo["label"] = "Waiters (" .. waiters .. ")"
    for i=1, waiters do
        local thread = 0

        thread, buffer = extract_threadID(buffer)

        waiterinfo[i] = {
            ["Thread ID"] = thread
        }
    end

    return {
        ["label"] = "MonitorInfo Reply",
        ["Entries"] = entryCount,
        ["Waiters"] = waiterinfo
    }
end

-- InvokeMethod Command (6)
-- Request
function dissect_invokeobjectmethod_request(buffer)
    local object = 0
    local thread = 0
    local clazz = 0
    local methodID = 0
    local arguments = 0
    local argumentinfo = {}
    local options = 0

    object, buffer = extract_objectID(buffer)
    thread, buffer = extract_threadID(buffer)
    clazz, buffer = extract_classID(buffer)
    arguments, buffer = extract_int(buffer)
    argumentinfo["label"] = "Arguments (" .. arguments .. ")"
    for i=1, arguments do
        local value = 0

        value, buffer = extract_value(buffer)

        argumentinfo[i] = {
            ["Value"] = value
        }
    end
    options, buffer = extract_int(buffer)

    return {
        ["label"] = "InvokeMethod Request",
        ["Object ID"] = object,
        ["Thread ID"] = thread,
        ["Class ID"] = clazz,
        ["Method ID"] = methodID,
        ["Arguments"] = argumentinfo,
        ["Options"] = options
    }
end
-- Reply
function dissect_invokeobjectmethod_reply(buffer)
    local returnValue = 0
    local exception = 0

    returnValue, buffer = extract_value(buffer)
    exception, buffer = extract_untagged_value(buffer)

    return {
        ["label"] = "InvokeMethod Reply",
        ["Return Value"] = returnValue,
        ["Exception"] = exception
    }
end

-- DisableCollection Command (7)
-- Request
function dissect_disableobjectcollection_request(buffer)
    local object = 0

    object, buffer = extract_objectID(buffer)

    return {
        ["label"] = "DisableCollection Request",
        ["Object ID"] = object
    }
end
-- No Reply

-- EnableCollection Command (8)
-- Request
function dissect_enableobjectcollection_request(buffer)
    local object = 0

    object, buffer = extract_objectID(buffer)

    return {
        ["label"] = "EnableCollection Request",
        ["Object ID"] = object
    }
end
-- No Reply

-- IsCollected Command (9)
-- Request
function dissect_isobjectcollected_request(buffer)
    local object = 0

    object, buffer = extract_objectID(buffer)

    return {
        ["label"] = "IsCollected Request",
        ["Object ID"] = object
    }
end
-- Reply
function dissect_isobjectcollected_reply(buffer)
    local isCollected = 0

    isCollected, buffer = extract_boolean(buffer)

    return {
        ["label"] = "IsCollected Reply",
        ["Is Collected"] = isCollected
    }
end

-- ReferringObjects Command (10)
-- Request
function dissect_objectreferringobjects_request(buffer)
    local object = 0
    local maxReferrers = 0

    object, buffer = extract_objectID(buffer)
    maxReferrers, buffer = extract_int(buffer)

    return {
        ["label"] = "ReferringObjects",
        ["Object ID"] = object,
        ["Max Referrers"] = maxReferrers
    }
end
-- Reply
function dissect_objectreferringobjects_reply(buffer)
    local referringObjects = 0
    local objectinfo = {}

    referringObjects, buffer = extract_int(buffer)
    objectinfo["label"] = "Referring Objects (" .. referringObjects .. ")"
    for i=1, referringObjects do
        local instance = 0

        instance, buffer = extract_tagged_objectID(buffer)

        objectinfo[i] = {
            ["Instance"] = instance
        }
    end

    return {
        ["label"] = "ReferringObjects Reply",
        ["Referring Objects"] = objectinfo
    }
end