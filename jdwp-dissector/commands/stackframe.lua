package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
tags = require("constants.jdwp-tag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- GetValues Command (1)
-- Request
function dissect_stackframe_getvalues_request(buffer)
    local thread = 0
    local frame = 0
    local slots = 0
    local slotinfo = {}

    thread, buffer = extract_threadID(buffer)
    frame, buffer = extract_frameID(buffer)
    slots, buffer = extract_int(buffer)
    slotinfo["label"] = "Values (" .. slots .. ")"
    for i=1, slots do
        local slot = 0
        local sigbyte = 0

        slot, buffer = extract_int(buffer)
        sigbyte, buffer = extract_byte(buffer)

        slotinfo[i] = {
            ["Slot"] = slot,
            ["Tag"] = tags[sigbyte]
        }
    end

    return {
        ["label"] = "GetValues Request",
        ["Thread ID"] = thread,
        ["Frame ID"] = frame,
        ["Values"] = slotinfo
    }
end
-- Reply
function dissect_stackframe_getvalues_reply(buffer)
    local values = 0
    local valueinfo = {}
    
    values, buffer = extract_int(buffer)
    valueinfo["label"] = "Values (" .. values .. ")"
    for i=1, values do
        local slotValue = 0

        slotValue, buffer = extract_value(buffer)

        valueinfo[i] = {
            ["Value"] = slotValue
        }
    end

    return {
        ["label"] = "GetValues Reply",
        ["Values"] = valueinfo
    }
end

-- SetValues Command (2)
-- Request
function dissect_stackframe_setvalues_request(buffer)
    local thread = 0
    local frame = 0
    local slotValues = 0
    local slotinfo = {}

    thread, buffer = extract_threadID(buffer)
    frame, buffer = extract_frameID(bufferf)
    slotValues, buffer = extract_int(buffer)
    slotinfo["label"] = "Values (" .. slotValues .. ")"
    for i=1, slotValues do
        local slot = 0
        local value = 0

        slot, buffer = extract_int(buffer)
        value, buffer = extract_value(buffer)

        slotinfo[i] = {
            ["Slot"] = slot,
            ["Value"] = value
        }
    end

    return {
        ["label"] = "SetValues Request",
        ["Thread ID"] = thread,
        ["Frame ID"] = frame,
        ["Values"] = slotinfo
    }
end
-- No Reply

-- ThisObject Command (3)
-- Request
function dissect_stackframe_thisobject_request(buffer)
    local thread = 0
    local frame = 0

    thread, buffer = extract_threadID(buffer)
    frame, buffer = extract_frameID(buffer)

    return {
        ["label"] = "ThisObject Request",
        ["Thread ID"] = thread,
        ["Frame ID"] = frame
    }
end
-- Reply
function dissect_stackframe_thisobject_request(buffer)
    local objectThis = 0

    objectThis, buffer = extract_tagged_objectID(buffer)

    return {
        ["label"] = "ThisObject Reply",
        ["Object ID"] = objectThis
    }
end

-- PopFrames Command (4)
-- Request
function dissect_stackframe_popframes_request(buffer)
    local thread = 0
    local frame = 0

    thread, buffer = extract_threadID(buffer)
    frame, buffer = extract_frameID(buffer)

    return {
        ["label"] = "PopFrames Request",
        ["Thread ID"] = thread,
        ["Frame ID"] = frame
    }
end
-- No Reply