package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Signature Command (1)
-- Request
function dissect_signature_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Signature Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_signature_reply(buffer)
    local signature = "" 

    signature, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Signature Reply",
        ["Signature"] = signature
    }
end

-- ClassLoader Command (2)
-- Request
function dissect_class_loader_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ClassLoader Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_class_loader_reply(buffer)
    local classLoaderID = 0

    classLoaderID, buffer = extract_classLoaderID(buffer)

    return {
        ["label"] = "ClassLoader Reply",
        ["Class Loader ID"] = classLoaderID
    }
end

-- Modifiers Command (3)
-- Request
function dissect_modifiers_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Modifiers Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_modifiers_reply(buffer)
    local modBits = 0

    modBits, buffer = extract_int(buffer)

    return {
        ["label"] = "Modifiers Reply",
        ["Modbits"] = modBits
    }
end

-- Fields Command (4)
-- Request
function dissect_fields_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Fields Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_fields_reply(buffer)
    local declared = 0
    local fields = {}

    declared, buffer = extract_int(buffer)
    for i=1, declared do
        local fieldID = 0
        local name = ""
        local signature = ""
        local modBits = 0

        fieldID, buffer = extract_fieldID(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        modBits, buffer = extract_int(buffer)

        fields[i] = {
            ["label"] = "Field " .. i,
            ["Field ID"] = fieldID,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Mod Bits"] = modBits
        }
    end

    return {
        ["label"] = "Fields Reply",
        ["Fields"] = fields
    }

end

-- Methods Command (5)
-- Request
function dissect_methods_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Methods Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_methods_reply(buffer)
    local declared = 0
    local methods = {}

    declared, buffer = extract_int(buffer)
    for i=1, declared do
        local methodID = 0
        local name = ""
        local signature = ""
        local genericSignature = ""
        local modBits = 0

        methodID, buffer = extract_methodID(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        genericSignature, buffer = extract_string(buffer)
        modBits, buffer = extract_int(buffer)

        methods[i] = {
            ["label"] = "Method " .. i,
            ["Method ID"] = methodID,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Mod Bits"] = modBits
        }
    end

    return {
        ["label"] = "Methods Reply",
        ["Methods"] = methods
    }
end

function dissect_get_values_request(buffer)

end

function dissect_get_values_reply(buffer)

end

-- SourceFile Command (7)
-- Request
function dissect_source_file_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "SourceFile Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_source_file_reply(buffer)
    local sourceFile = ""

    sourceFile, buffer = extract_string(buffer)

    return {
        ["label"] = "SourceFile Reply",
        ["Source File"] = sourceFile
    }
end

-- NestedTypes Command (8)
-- Request
function dissect_nested_types_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Nested Types Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_nested_types_reply(buffer)
    local classes = 0
    local nestedTypes = {}

    classes, buffer = extract_int(buffer)
    for i=1, classes do
        local refTypeTag = 0
        local referenceTypeID = 0

        refTypeTag, buffer = extract_byte(buffer)
        referenceTypeID, buffer = extract_referenceTypeID(buffer)

        nestedTypes[i] = {
            ["label"] = "Class " .. i,
            ["Reference Type"] = typetags[refTypeTag],
            ["Reference Type ID"] = referenceTypeID
        }
    end

    return {
        ["label"] = "NestedTypes Reply",
        ["Nested Types"] = nestedTypes
    }
end

-- Status Command (9)
-- Request
function dissect_referencetypestatus_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Status Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_referencetypestatus_reply(buffer)
    local classstatus = 0

    classstatus, buffer = extract_int(buffer)

    return {
        ["label"] = "Status Reply",
        ["Status"] = classstates[status]
    }
end

-- Interfaces Command (10)
-- Request
function dissect_interfaces_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Interfaces Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_interfaces_reply(buffer)
    local interfaces = 0
    local interfaceinfo = {}

    interfaces, buffer = extract_int(buffer)
    for i=1, interfaces do
        local interfaceID = 0

        interfaceID, buffer = extract_interfaceID(buffer)

        interfaceinfo[i] = {
            ["label"] = "Interface " .. i,
            ["Interface Type"] = interfaceID
        }
    end

    return {
        ["label"] = "Interfaces Reply",
        ["Interfaces"] = interfaceinfo
    }
end

-- ClassObject Command (11)
-- Request
function dissect_class_object_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ClassObject Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_class_object_reply(buffer)
    local classObjectID = 0

    classObjectID, buffer = dissect_classObjectID(buffer)

    return {
        ["label"] = "ClassObject Reply",
        ["Class Object ID"] = classObjectID
    }
end

-- SourceDebugExtension Command (12)
-- Request
function dissect_source_debug_extension_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "SourceDebugExtension Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_source_debug_extension_reply(buffer)
    local extension = ""

    extension, buffer = extract_string(buffer)

    return {
        ["label"] = "SourceDebugExtension Reply",
        ["Extension"] = extension
    }
end

-- SignatureWithGeneric Command (13)
-- Request
function dissect_signature_with_generic_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "SignatureWithGeneric Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_signature_with_generic_reply(buffer)
    local signature = ""
    local genericSignature = ""

    signature, buffer = extract_string(buffer)
    genericSignature, buffer = extract_string(buffer)

    return {
        ["label"] = "SignatureWithGeneric Reply",
        ["Signature"] = signature,
        ["Generic Signature"] = genericSignature
    }
end

-- FieldsWithGeneric Command (4)
-- Request
function dissect_fields_with_generic_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "Fields Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_fields_with_generic_reply(buffer)
    local declared = 0
    local fields = {}

    declared, buffer = extract_int(buffer)
    for i=1, declared do
        local fieldID = 0
        local name = ""
        local signature = ""
        local genericSignature = ""
        local modBits = 0

        fieldID, buffer = extract_fieldID(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        genericSignature, buffer = extract_string(buffer)
        modBits, buffer = extract_int(buffer)

        fields[i] = {
            ["label"] = "Field " .. i,
            ["Field ID"] = fieldID,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Generic Signature"] = genericSignature,
            ["Mod Bits"] = modBits
        }
    end

    return {
        ["label"] = "Fields Reply",
        ["Fields"] = fields
    }
end

-- MethodsWithGeneric Command (15)
-- Request
function dissect_methods_with_generic_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "MethodsWithGeneric Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_methods_with_generic_reply(buffer)
    local declared = 0
    local methods = {}

    declared, buffer = extract_int(buffer)
    for i=1, declared do
        local methodID = 0
        local name = ""
        local signature = ""
        local genericSignature = ""
        local modBits = 0

        methodID, buffer = extract_methodID(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        genericSignature, buffer = extract_string(buffer)
        modBits, buffer = extract_int(buffer)

        methods[i] = {
            ["label"] = "Method " .. i,
            ["Method ID"] = methodID,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Generic Signature"] = genericSignature,
            ["Mod Bits"] = modBits
        }
    end

    return {
        ["label"] = "MethodsWithGeneric Reply",
        ["Methods"] = methods
    }
end

-- Instances Command (16)
-- Request
function dissect_instances_request(buffer)
    local referenceTypeID = 0
    local maxInstances = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)
    maxInstances, buffer = extract_int(buffer)

    return {
        ["label"] = "Instances Request",
        ["Reference Type ID"] = referenceTypeID,
        ["Max Instances"] = maxInstances
    }
end
-- Reply
function dissect_instances_reply(buffer)
    local instances = 0
    local instanceinfo = {}

    instances, buffer = extract_int(buffer)
    for i=1, instances do
        local instance = 0

        instance, buffer = extract_taggedObjectID(buffer)

        methods[i] = {
            ["label"] = "Instance " .. i,
            ["Instance (tagged-ObjectID)"] = instance
        }
    end

    return {
        ["label"] = "Instances Reply",
        ["Instances"] = instanceinfo
    }
end

-- ClassFileVersion Command (17)
-- Request
function dissect_classfile_version_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ClassFileVersion Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_classfile_version_reply(buffer)
    local majorVersion = 0
    local minorVersion = 0

    majorVersion, buffer  = extract_int(buffer)
    minorVersion, buffer  = extract_int(buffer)

    return {
        ["label"] = "ClassFileVersion Reply",
        ["Major Version"] = majorVersion,
        ["Minor Version"] = minorVersion
    }
end

-- ConstantPool Command (18)
-- Request
function dissect_constant_pool_request(buffer)
    local referenceTypeID = 0

    referenceTypeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ConstantPool Request",
        ["Reference Type ID"] = referenceTypeID
    }
end
-- Reply
function dissect_cosntant_pool_reply(buffer)
    local count = 0
    local bytes = 0
    local rawBytes = {}

    count, buffer = extract_int(buffer)
    bytes, buffer = extract_int(buffer)

    for i=1, bytes do
        local cpbytes = 0

        cpbytes, buffer = extract_byte(buffer)

        rawBytes[i] = cpbytes
    end

    return {
        ["label"] = "ConstantPool Reply",
        ["Count"] = count,
        ["Raw Bytes"] = rawBytes
    }
end
