package.path = package.path .. ";../?.lua" .. ";..\\?.lua"
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Length Command (1)
-- Request
function dissect_length_request(buffer)
    local arrayObject = 0

    arrayObject, buffer = extract_arrayID(buffer)

    return {
        ["label"] = "Length Request",
        ["Array Object ID"] = arrayObject
    }
end
-- Reply
function dissect_length_reply(buffer)
    local arrayLength = 0

    arrayLength, buffer = extract_int(buffer)

    return {
        ["label"] = "Length Reply",
        ["Array Length"] = arrayLength
    }
end

-- GetValues Command (2)
-- Request
function dissect_getvalues_request(buffer)
    local arrayObject = 0
    local firstIndex = 0
    local length = 0

    arrayObject, buffer = extract_arrayID(buffer)
    firstIndex, buffer = extract_int(buffer)
    length, buffer = extract_int(buffer)

    return {
        ["label"] = "GetValues Request",
        ["Array Object ID"] = arrayObject,
        ["First Index"] = firstIndex,
        ["Length"] = length
    }
end
-- Reply
function dissect_getvalues_reply(buffer)
    local arrayregion = {}

    arrayregion, buffer = extract_arrayregion(buffer)

    return {
        ["label"] = "GetValues Reply",
        ["Values"] = arrayregion
    }
end

-- SetValues Command (3)
-- Request
function dissect_setvalues_request(buffer)
    local arrayObject = 0
    local firstIndex = 0
    local values = 0
    local valueinfo = {}

    arrayObject, buffer = extract_arrayID(buffer)
    firstIndex, buffer = extract_int(buffer)
    values, buffer = extract_int(buffer)
    valueinfo["label"] = "Values (" .. values .. ")"
    for i=1, values do
        local value = 0

        value, buffer = extract_untagged_value(buffer)

        valueinfo[i] {
            ["Value"] = value
        }
    end

    return {
        ["label"] = "SetValues Request",
        ["Array Object ID"] = arrayObject,
        ["First Index"] = firstIndex,
        ["Values"] = valueinfo
    }
end
-- No Reply