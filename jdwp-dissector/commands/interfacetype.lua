package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
invokeoptions = require("constants.jdwp-invokeoptions")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- InvokeMethod Command (1)
-- Request
function dissect_invokeinterfacemethod_request(buffer)
    local clazz = 0
    local thread = 0
    local methodID = 0
    local arguments = 0
    local argumentinfo = {}
    local options = 0

    clazz, buffer  = extract_interfaceID(buffer)
    thread, buffer = extract_threadID(buffer)
    methodID, buffer = extract_methodID(buffer)
    arguments, buffer = extract_int(buffer)
    argumentinfo["label"] = "Arguments (" .. i .. ")"
    for i=1, arguments do
        local arg = 0

        arg, buffer = extract_value(buffer)

        argumentinfo[i] = {
            ["label"] = "Argument " .. i,
            ["Value"] = arg
        }
    end
    return {
        ["label"] = "InvokeMethod Request",
        ["Interface ID"] = clazz,
        ["Method ID"] = methodID,
        ["Thread ID"] = thread,
        ["Arguments"] = argumentinfo,
        ["Options"] = invokeoptions[options]
    }
end
-- Reply
function dissect_invokeinterfacemethod_reply(buffer)
    local returnValue = 0
    local exception = 0

    returnValue, buffer  = extract_value(buffer)
    exception = extract_tagged_objectID(buffer)

    newArray, buffer = extract_tagged_objectID(buffer)

    return {
        ["label"] = "InvokeMethod Reply",
        ["Return Value"] = returnValue,
        ["Exception"] = exception
    }
end
