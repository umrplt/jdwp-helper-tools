package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
eventkinds = require("constants.jdwp-eventkind")
suspendpolicies = require("constants.jdwp-suspendpolicy")
require("jdwp-sizes")
require("jdwp-fieldextractors")

modkinds = {
    [1] = function(buffer) 
        local count = 0

        count, buffer = extract_int(buffer)

        return {
            ["Modifier"] = "Limit",
            ["Count"] = count
        }, buffer
    end,
    [2] = function (buffer)
        local exprID = 0

        exprID, buffer = extract_int(buffer)

        return {
            ["Modifier"] = "For the future",
            ["Expression ID"] = exprID
        }, buffer
    end,
    [3] = function (buffer)
        local thread = 0

        thread, buffer = extract_threadID(buffer)

        return {
            ["Modifier"] = "Required thread",
            ["Thread"] = thread
        }, buffer
    
    end,
    [4] = function (buffer)
        local clazz = 0

        clazz, buffer = extract_referenceTypeID(buffer)

        return {
            ["Modifier"] = "Required class",
            ["Required Class"] = clazz
        }, buffer
    end,
    [5] = function (buffer)
        local classPattern = ""

        classPattern, buffer = extract_string(buffer)

        return {
            ["Modifier"] = "Required class pattern",
            ["Class Pattern"] = classPattern
        }, buffer
    end,
    [6] = function (buffer)
        local classPattern = ""

        classPattern, buffer = extract_string(buffer)

        return {
            ["Modifier"] = "Disallowed class pattern",
            ["Class Pattern"] = classPattern
        }, buffer
    end,
    [7] = function (buffer)
        local location = 0

        location, buffer = extract_location(buffer)

        return {
            ["Modifier"] = "Required Location",
            ["Location"] = location
        }, buffer
    end,
    [8] = function (buffer)
        local exceptionOrNull = 0
        local caught = 0
        local uncaught = 0

        exceptionOrNull, buffer = extract_referenceTypeID(buffer)
        caught, buffer = extract_boolean(buffer)
        uncaught, buffer = extract_boolean(buffer)

        return {
            ["Modifier"] = "Exception restriction",
            ["Exception"] = exceptionOrNull,
            ["Report Caught"] = caught,
            ["Report Uncaught"] = uncaught
        }, buffer
    end,
    [9] = function (buffer)
        local declaring = 0
        local fieldID = 0

        declaring, buffer = extract_referenceTypeID(buffer)
        fieldID, buffer = extract_fieldID(buffer)

        return {
            ["Modifier"] = "Events on field restriction",
            ["Declaring Class"] = declaring,
            ["Field"] = fieldID
        }, buffer
    end,
    [10] = function (buffer)
        local threadID = 0
        local size = 0
        local depth = 0

        threadID, buffer = extract_threadID(buffer)
        size, buffer = extract_int(buffer)
        depth, buffer = extract_int(buffer)

        return {
            ["Modifier"] = "Events on thread restriction",
            ["Thread"] = threadID,
            ["Size"] = size,
            ["Depth"] = depth
        }, buffer
    end,
    [11] = function (buffer)
        local instance = 0

        instance, buffer = extract_objectID(buffer)

        return {
            ["Modifier"] = "Events on instance restriction",
            ["Instance"] = instance
        }, buffer
    end,
    [12] = function (buffer)
        local sourceNamePattern = ""

        sourceNamePattern, buffer = extract_string(buffer)

        return {
            ["Modifier"] = "Required source name pattern",
            ["Class Pattern"] = sourceNamePattern
        }, buffer
    end
}

-- Set Command (1)
-- Request
function dissect_set_request(buffer)
    local eventKind = 0
    local supendPolicy = 0
    local modifiers = 0
    local modinfo = {}
    
    eventKind, buffer = extract_byte(buffer)
    supendPolicy, buffer = extract_byte(buffer)
    modifiers, buffer = extract_int(buffer)
    modinfo["label"] = "Modifiers (" .. modifiers .. ")"
    for i=1, modifiers do
        local modKind = 0

        modKind, buffer = extract_byte(buffer)

        modinfo[i], buffer = modkinds[modKind](buffer)
        modinfo[i]["label"] = "Modifier " .. i
    end

    return {
        ["label"] = "Set Request",
        ["Event Kind"] = eventkinds[eventKind],
        ["Suspend Policy"] = suspendpolicies[supendPolicy],
        ["Modifiers"] = modinfo
    }
end
-- Reply
function dissect_set_reply(buffer)
    local requestID = 0

    requestID, buffer = extract_int(buffer)

    return {
        ["label"] = "Set Reply",
        ["Request ID"] = requestID
    }
end

-- Clear Command (2)
-- Request
function dissect_clear_request(buffer)
    local eventKind = 0
    local requestID = 0

    eventKind, buffer = extract_byte(buffer)
    requestID, buffer = extract_int(buffer)

    return {
        ["label"] = "Clear Request",
        ["Event Kind"] = eventkinds[eventKind],
        ["Request ID"] = requestID
    }
end
-- No Reply

-- ClearAllBreakpoints Command (3)
-- No Request
-- No Reply