package.path = package.path .. ";../?.lua" 
threadstates = require("constants.jdwp-threadstatus")
suspendstates = require("constants.jdwp-suspendstatus")
require("jdwp-fieldextractors")

-- Name Command (1)
-- Request
function dissect_name_request(buffer)
    local thread = 0
    
    thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "Name Request",
        ["Thread ID"] = thread
    }
end
-- Reply
function dissect_name_reply(buffer)
    local threadName = ""

    threadName, buffer = extract_string(buffer)

    return {
        ["label"] = "Name Reply",
        ["Name"] = threadName
    }
end

-- Suspend Command (2)
-- Request
function dissect_suspend_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "Suspend Request",
        ["Thread ID"] = thread
    }
end
-- No Reply

-- Resume Command (3) 
-- Request
function dissect_resume_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "Resume Request",
        ["Thread ID"] = thread
    }
end
-- No Reply

-- Status Command (4)
-- Request
function dissect_threadstatus_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "Status Request",
        ["Thread ID"] = thread
    }
end
-- Reply
function dissect_threadstatus_reply(buffer)
    local threadStatus = 0
    local suspendStatus = 0

    threadStatus, buffer = extract_int(buffer)
    suspendStatus, buffer = extract_int(buffer)

    return {
        ["label"] = "Status Reply",
        ["Thread Status"] = threadstates[threadStatus],
        ["Suspend Status"] = suspendstates[suspendStatus]
    }
end

-- ThreadGroup Command (5)
-- Request
function dissect_threadgroup_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "ThreadGroup Request",
        ["Thread ID"] = thread
    }
end
-- Reply
function dissect_threadgroup_reply(buffer)
    local group = 0

    group, buffer = extract_threadGroupID(buffer)

    return {
        ["label"] = "ThreadGroup Reply",
        ["Thread Group ID"] = group
    }
end

-- Frames Command (6)
-- Request
function dissect_frames_request(buffer)
    local thread = 0
    local startFrame = 0
    local length = 0

    thread, buffer = extract_threadID(buffer)
    startFrame, buffer = extract_int(buffer)
    length, buffer= extract_int(buffer)

    return {
        ["label"] = "Frames Request",
        ["Thread ID"] = thread,
        ["Start Frame"] = startFrame,
        ["Length"] = length
    }
end
-- Reply
function dissect_frames_reply(buffer)
    local frames = 0
    local frameinfo = {}

    frames, buffer = extract_int(buffer)
    frameinfo["label"] = "Frames (" .. frames .. ")"
    for i=1, frames do
        local frameID = 0
        local location = 0

        frameID, buffer = extract_frameID(buffer)
        location, buffer = extract_location(buffer)

        frameinfo[i] = {
            ["label"] = "Frame " .. i,
            ["Frame ID"] = frameID,
            ["Location"] = location
        }
    end

    return {
        ["label"] = "Frames Reply",
        ["Frames"] = frameinfo
    }
end

-- FrameCount Command (7)
-- Request
function dissect_framecount_request(buffer)
   local thread = 0

   thread, buffer = extract_threadID(buffer)

    return {
        ["label"] = "FrameCount Request",
        ["Thread ID"] = thread
    }
end
-- Reply
function dissect_framecount_reply(buffer)
    local frameCount = 0

    frameCount, buffer = extract_int(buffer)

    return {
        ["label"] = "FrameCount Reply",
        ["Frame Count"] = frameCount
    }
end

-- OwnedMonitors Command (8)
-- Request
function dissect_ownedmonitors_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)
 
     return {
         ["label"] = "OwnedMonitors Request",
         ["Thread ID"] = thread
     }
end
-- Reply
function dissect_ownedmonitors_reply(buffer)
    local owned = 0
    local monitors = {}

    owned, buffer = extract_int(buffer)
    monitors["label"] = "Monitors (" .. owned .. ")"
    for i=1, owned do
        local monitor = 0

        monitor, buffer = extract_tagged_objectID(buffer)

        monitors[i] = {
            ["label"] = "Monitor " .. i,
            ["Monitor"] = monitor
        }
    end

    return {
        ["label"] = "OwnedMonitors Reply",
        ["Monitors"] = monitors 
    }
end

-- CurrentContendedMonitor Command (9)
-- Request
function dissect_currentcontendedmonitor_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)
 
     return {
         ["label"] = "CurrentContendedMonitor Request",
         ["Thread ID"] = thread
     }
end
-- Reply
function dissect_currentcontendedmonitor_reply(buffer)
    local monitor = 0

    monitor, buffer = extract_tagged_objectID(buffer)

    return {
        ["label"] = "CurrentContendedMonitor Reply",
        ["Monitor"] = monitor
    }
end

-- Stop Command (10)
-- Request
function dissect_stop_request(buffer)
    local thread = 0
    local throwable = 0

    thread, buffer = extract_threadID(buffer)
    throwable, buffer = extract_objectID(buffer)
 
     return {
         ["label"] = "Stop Request",
         ["Thread ID"] = thread,
         ["Throwable"] = throwable
     }
end
-- No reply

-- Interrupt Command (11)
-- Request
function dissect_interrupt_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)
 
     return {
         ["label"] = "Interrupt Request",
         ["Thread ID"] = thread
     }
end
-- No reply 

-- SuspendCount Command (12)
-- Request
function dissect_suspendcount_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)
 
     return {
         ["label"] = "SuspendCount Request",
         ["Thread ID"] = thread
     }
end
-- Reply
function dissect_suspendcount_reply(buffer)
    local suspendCount = 0

    suspendCount, buffer = extract_int(buffer)

    return {
        ["label"] = "SuspendCount Request",
        ["Suspend Count"] = suspendCount
    }
end

-- OwnedMonitorsStackDepthInfo Command (13)
-- Request
function dissect_ownedmonitorsstackdepthinfo_request(buffer)
    local thread = 0

    thread, buffer = extract_threadID(buffer)
 
     return {
         ["label"] = "OwnedMonitorsStackDepthInfo Request",
         ["Thread ID"] = thread
     }     
end
-- Reply
function dissect_ownedmonitorsstackdepthinfo_reply(buffer)
    local owned = 0
    local monitors = {}

    owned, buffer = extract_int(buffer)
    monitors["label"] = "Monitors (" .. owned .. ")"
    for i=1, owned do
        local monitor = 0
        local stackDepth = 0

        monitor, buffer = extract_tagged_objectID(buffer)
        stackDepth, buffer = extract_int(buffer)

        monitors[i] = {
            ["label"] = "Monitor " .. i,
            ["Stack Depth"] = stackDepth
        }
    end

    return {
        ["label"] = "OwnedMonitorsStackDepthInfo Request",
        ["Monitors"] = monitors
    } 
end

-- ForceEarlyReturn Command (14)
-- Request
function dissect_forceearlyreturn_request(buffer)
    local thread = 0
    local value = 0
    
    thread, buffer = extract_threadID(buffer)
    value, buffer = extract_value(buffer)

    return {
        ["label"] = "ForceEarlyReturn Request",
        ["Thread ID"] = thread,
        ["Value"] = value
    }
end
-- No Reply