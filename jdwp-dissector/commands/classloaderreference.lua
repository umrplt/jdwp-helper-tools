package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- VisibleClasses Command (1)
-- Request
function dissect_visibleclasses_request(buffer)
    local classLoaderObject = 0

    classLoaderObject, buffer = extract_classLoaderID(buffer)

    return {
        ["label"] = "VisibleClasses Request",
        ["Classloader Object ID"] = classLoaderObject
    }
end
-- Reply
function dissect_visibleclasses_reply(buffer)
    local classes = 0
    local classinfo = {}

    classes, buffer = extract_int(buffer)
    classinfo["label"] = "Classes (" .. classes .. ")"
    for i=1, classes do
        local refTypeTag = 0
        local typeID = 0

        refTypeTag, buffer = extract_byte(buffer)
        typeID, buffer = extract_referenceTypeID(buffer)

        classinfo[i] = {
            ["Reference Type"] = typetags[refTypeTag],
            ["Reference Type ID"] = typeID
        }
    end
end