package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
eventkinds = require("constants.jdwp-eventkind")
suspendpolicies = require("constants.jdwp-suspendpolicy")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- ReflectedType Command (1)
-- Request
function dissect_reflectedtype_request(buffer)
    local classObject = 0
    
    classObject, buffer = extract_classObjectID(buffer)

    return {
        ["label"] = "ReflectedType Request",
        ["Class Object ID"] = classObject
    }
end
-- Reply
function dissect_reflectedtype_reply(buffer)
    local  refTypeTag = 0
    local  typeID = 0

    refTypeTag, buffer = extract_byte(buffer)
    typeID, buffer = extract_referenceTypeID(buffer)

    return {
        ["label"] = "ReflectedType Reply",
        ["Reference Type"] = typetags[refTypeTag],
        ["Reference Type ID"] = typeID
    }
end