package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
invokeoptions = require("constants.jdwp-invokeoptions")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- NewInstance Command (1)
-- Request
function dissect_newarrayinstance_request(buffer)
    local arrType = 0
    local length = 0

    return {
        ["label"] = "NewInstance Request",
        ["Array Type ID"] = arrType,
        ["Length"] = length
    }
end
-- Reply
function dissect_newarrayinstance_reply(buffer)
    local newArray = 0

    newArray, buffer = extract_tagged_objectID(buffer)

    return {
        ["label"] = "NewInstance Reply",
        ["New Array"] = newArray
    }
end
