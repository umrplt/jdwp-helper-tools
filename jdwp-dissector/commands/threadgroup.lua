package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Name Command (1)
-- Request
function dissect_threadgroup_name_request(buffer)
    local group = 0

    group, buffer = extract_threadGroupID(buffer)

    return {
        ["label"] = "Name Request",
        ["Threadgroup ID"] = group
    }
end
-- Reply
function dissect_threadgroup_name_reply(buffer)
    local groupName = ""

    groupName, buffer = extract_string(buffer)

    return {
        ["label"] = "Name Reply",
        ["Group Name"] = groupName
    }
end

-- Parent Command (2)
-- Request
function dissect_parent_request(buffer)
    local group = 0

    group, buffer = extract_threadGroupID(buffer)

    return {
        ["label"] = "Parent Request",
        ["Threadgroup ID"] = group
    }
end
-- Reply
function dissect_parent_request(buffer)
    local group = 0

    group, buffer = extract_threadGroupID(buffer)

    return {
        ["label"] = "Parent Reply",
        ["Parent Threadgroup ID"] = group
    }
end

-- Children Command (3)
-- Request
function dissect_children_request(buffer)
    local group = 0

    group, buffer = extract_threadGroupID(buffer)

    return {
        ["label"] = "Children Request",
        ["Threadgroup ID"] = group
    }
end
-- Reply
function dissect_children_reply(buffer)
    local childThreads = 0
    local childthreadinfo = {}
    local childGroups = 0
    local childgroupinfo = {}

    childThreads, buffer = extract_int(buffer)
    childthreadinfo["label"] = "Child Threads (" .. childThreads .. ")"
    for i=1, childThreads do
        local threadID = 0

        threadID, buffer = extract_threadID(buffer)

        childthreadinfo[i] = {
            ["Child Thread ID"] = threadID
        }
    end

    childGroups, buffer = extract_int(buffer)
    childgroupinfo["label"] = "Child Thread Groups (" .. childGroups .. ")"
    for i=1, childGroups do
        local childGroup = 0

        childGroup, buffer = extract_threadGroupID(buffer)

        childgroupinfo[i] = {
            ["Child Group ID"] = childGroup
        }
    end

    return {
        ["label"] = "Children Reply",
        ["Child Threads"] = childthreadinfo,
        ["Child Groups"] = childgroupinfo
    }
end