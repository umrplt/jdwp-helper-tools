package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Nothing to do here for now