package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- LineTable Command (1)
-- Request
function dissect_linetable_request(buffer)
    local refType = 0
    local methodID = 0

    refType, buffer = extract_referenceTypeID(buffer)
    methodID, buffer = extract_methodID(buffer)

    return {
        ["label"] = "LineTable Request",
        ["Class ID"] = refType,
        ["Method ID"] = methodID
    }
end
-- Reply
function dissect_linetable_reply(buffer)
    local startindex = 0
    local endindex = 0
    local lines = 0
    local lineinfo = {}

    startindex, buffer = extract_int(buffer)
    endindex, buffer = extract_int(buffer)
    lines, buffer = extract_int(buffer)
    lineinfo["label"] = "Lines (" .. slots .. ")"
    for i=1, lines do
        local codeIndex = 0
        local lineNumber = 0

        codeIndex, buffer = extract_long(buffer)
        lineNumber, buffer = extract_int(buffer)

        variables[i] = {
            ["label"] = "Line " + i,
            ["Code Index"] = codeIndex,
            ["Line Number"] = lineNumber
        }
    end

    return {
        ["label"] = "LineTable Reply",
        ["Start"] = startindex,
        ["End"] = endindex,
        ["Lines"] = lineinfo
    }
end

-- VariableTable Command(2)
-- Request
function dissect_linetable_request(buffer)
    local refType = 0
    local methodID = 0

    refType, buffer = extract_referenceTypeID(buffer)
    methodID, buffer = extract_methodID(buffer)

    return {
        ["label"] = "VariableTable Request",
        ["Class ID"] = refType,
        ["Method ID"] = methodID
    }
end
-- Reply
function dissect_variabletable_reply(buffer)
    local argCnt = 0
    local slots = 0
    local variables = {}

    argCnt, buffer = extract_int(buffer)
    slots, buffer = extract_int(buffer)
    variables["label"] = "Variables (" .. slots .. ")"
    for i=1, slots do
        local codeIndex = 0
        local name = ""
        local signature = ""
        local length = 0
        local slot = 0

        codeIndex, buffer = extract_long(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        length, buffer = extract_int(buffer)
        slot, buffer = extract_int(buffer)

        variables[i] = {
            ["label"] = "Variable " + i,
            ["Code Index"] = codeIndex,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Length"] = length,
            ["Slot"] = slot
        }
    end

    return {
        ["label"] = "VariableTable Reply",
        ["Word Count"] = argCnt,
        ["Variables"] = variables
    }
end

-- Bytecodes Command(3)
-- Request
function dissect_bytecodes_request(buffer)
    local refType = 0
    local methodID = 0

    refType, buffer = extract_referenceTypeID(buffer)
    methodID, buffer = extract_methodID(buffer)

    return {
        ["label"] = "Bytecodes Request",
        ["Class ID"] = refType,
        ["Method ID"] = methodID
    }
end
-- Reply
function dissect_bytecodes_reply(buffer)
    local bytes = 0
    local bytecodes = {}

    bytes, buffer = extract_int(buffer)
    bytecode["label"] = "Instructions (" .. bytes .. " Bytes)"
    for i=1, bytes do
        local bytecode = 0

        bytecode, buffer = extract_byte(buffer)

        bytecodes[i] = {
            ["label"] = "Bytecode " .. i,
            ["Instruction"] = bytecode
        }
    end

    return {
        ["label"] = "Bytecodes Reply",
        ["Bytecode"] = bytecode
    }
end

-- IsObsolete Command(4)
-- Request
function dissect_isobsolete_request(buffer)
    local refType = 0
    local methodID = 0

    refType, buffer = extract_referenceTypeID(buffer)
    methodID, buffer = extract_methodID(buffer)

    return {
        ["label"] = "IsObsolete Request",
        ["Class ID"] = refType,
        ["Method ID"] = methodID
    }
end
-- Reply
function dissect_isobsolete_reply(buffer)
    local isObsolete = 0

    isObsolete, buffer = extract_boolean(buffer)

    return {
        ["label"] = "IsObsolete Reply",
        ["Is Obsolete"] = isObsolete
    }
end

-- VariableTableWithGeneric Command(5)
-- Request
function dissect_variabletablewithgeneric_request(buffer)
    local refType = 0
    local methodID = 0

    refType, buffer = extract_referenceTypeID(buffer)
    methodID, buffer = extract_methodID(buffer)

    return {
        ["label"] = "VariableTableWithGeneric Request",
        ["Class ID"] = refType,
        ["Method ID"] = methodID
    }
end
-- Reply
function dissect_variabletablewithgeneric_reply(buffer)
    local argCnt = 0
    local slots = 0
    local variables = {}

    argCnt, buffer = extract_int(buffer)
    slots, buffer = extract_int(buffer)
    variables["label"] = "Variables (" .. slots .. ")"
    for i=1, slots do
        local codeIndex = 0
        local name = ""
        local signature = ""
        local genericSignature = ""
        local length = 0
        local slot = 0

        codeIndex, buffer = extract_long(buffer)
        name, buffer = extract_string(buffer)
        signature, buffer = extract_string(buffer)
        genericSignature, buffer = extract_string(buffer)
        length, buffer = extract_int(buffer)
        slot, buffer = extract_int(buffer)

        variables[i] = {
            ["label"] = "Variable " + i,
            ["Code Index"] = codeIndex,
            ["Name"] = name,
            ["Signature"] = signature,
            ["Generic Signature"] = genericsignature,
            ["Length"] = length,
            ["Slot"] = slot
        }
    end

    return {
        ["label"] = "VariableTableWithGeneric Reply",
        ["Word Count"] = argCnt,
        ["Variables"] = variables
    }
end