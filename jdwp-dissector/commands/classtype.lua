package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
invokeoptions = require("constants.jdwp-invokeoptions")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Superclass Command (1)
-- Request
function dissect_superclass_request(buffer)
    local clazz = 0

    clazz, buffer = extract_classID(buffer)

    return {
        ["label"] = "Superclass Request",
        ["Class ID"] = clazz
    }
end
-- Reply
function dissect_superclass_reply(buffer)
    local clazz = 0

    clazz, buffer = extract_classID(buffer)

    return {
        ["label"] = "Superclass Reply",
        ["Superclass ID"] = clazz
    }
end

-- SetValues Command (2)
-- Request
function dissect_setvalues_request(buffer)
    local clazz = 0
    local values = 0
    local valueinfo = {}

    clazz, buffer = extract_classID(buffer)
    values, buffer = extract_int(buffer)
    valueinfo["label"] = "Values (" .. values .. ")"
    for i=1, values do
        local fieldID = 0
        local value = 0

        fieldID, buffer = extract_fieldID(buffer)
        value, buffer = extract_untagged_value(buffer)

        valueinfo = {
            ["label"] = "Value " .. i,
            ["Field ID"] = fieldID,
            ["Value"] = value
        }
    end

    return {
        ["label"] = "SetValues Request",
        ["Class ID"] = clazz,
        ["Values"] = values 
    }
end
-- No Reply

-- InvokeMethod Command (3)
-- Request
function dissect_invokevirtualmethod_request(buffer)
    local clazz = 0
    local thread = 0
    local methodID = 0
    local arguments = 0
    local argumentinfo = {}
    local option = 0

    clazz, buffer = extract_classID(buffer)
    thread, buffer = extract_threadID(buffer)
    methodID, buffer = extract_methodID(buffer)
    arguments, buffer = extract_int(buffer)
    argumentinfo["label"] = "Arguments (" .. arguments .. ")"
    for i=1, arguments do
        local value = 0

        value, buffer = extract_value(buffer)

        argumentinfo[i] = {
            ["label"] = "Argument " .. i,
            ["Value"] = value
        }
    end
    options, buffer = extract_int(buffer)

    return {
        ["label"] = "InvokeMethod Request",
        ["Class ID"] = clazz,
        ["Method ID"] = methodID,
        ["Thread ID"] = thread,
        ["Arguments"] = argumentinfo,
        ["Options"] = invokeoptions[options]
    }
end
-- Reply
function dissect_invokevirtualmethod_reply(buffer)
    local returnValue = 0
    local exception = 0

    returnValue, buffer = extract_value(buffer)
    exception, buffer =  extract_tagged_objectID(buffer)

    return {
        ["label"] = "InvokeMethod Reply",
        ["Return Value"] = returnValue,
        ["Exception"] = exception
    }
end

-- NewInstance Command (4)
-- Request
function dissect_newclassinstance_request(buffer)
    local clazz = 0
    local thread = 0
    local methodID = 0
    local arguments = 0
    local argumentinfo = {}
    local option = 0

    clazz, buffer = extract_classID(buffer)
    thread, buffer = extract_threadID(buffer)
    methodID, buffer = extract_methodID(buffer)
    arguments, buffer = extract_int(buffer)
    argumentinfo["label"] = "Arguments (" .. arguments .. ")"
    for i=1, arguments do
        local value = 0

        value, buffer = extract_value(buffer)

        argumentinfo[i] = {
            ["label"] = "Argument " .. i,
            ["Value"] = value
        }
    end
    options, buffer = extract_int(buffer)

    return {
        ["label"] = "NewInstance Request",
        ["Class ID"] = clazz,
        ["Method ID"] = methodID,
        ["Thread ID"] = thread,
        ["Arguments"] = argumentinfo,
        ["Options"] = invokeoptions[options]
    }
end
-- Reply
function dissect_newclassinstance_reply(buffer)
    local newObject = 0
    local exception = 0

    newObject, buffer = extract_tagged_objectID(buffer)
    exception, buffer =  extract_tagged_objectID(buffer)

    return {
        ["label"] = "NewInstance Reply",
        ["New Object"] = newObject,
        ["Exception"] = exception
    }
end
