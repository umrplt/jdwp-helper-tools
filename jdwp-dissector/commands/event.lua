package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
eventkinds = require("constants.jdwp-eventkind")
require("jdwp-sizes")
require("jdwp-fieldextractors")

local eventKind_dispatch = {
    [VM_START] = function (buffer)
        local requestID = 0
        local thread = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)

        return {
            ["Event"] = eventkinds[VM_START],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread
        }, buffer
    end,
    [SINGLE_STEP] = function(buffer)
        local requestID = 0
        local thread = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[SINGLE_STEP],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location
        }, buffer
    end,
    [BREAKPOINT] = function(buffer)
        local requestID = 0
        local thread = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[BREAKPOINT],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location
        }, buffer
    end,
    [METHOD_ENTRY] = function (buffer)
        local requestID = 0
        local thread = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[METHOD_ENTRY],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location
        }, buffer
    end,
    [METHOD_EXIT] = function (buffer)
        local requestID = 0
        local thread = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[METHOD_EXIT],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location
        }, buffer
    end,
    [METHOD_EXIT_WITH_RETURN_VALUE] = function (buffer)
        local requestID = 0
        local thread = 0
        local location = 0
        local value = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        value, buffer = extract_value(buffer)
        
        return {
            ["Event"] = eventkinds[METHOD_EXIT_WITH_RETURN_VALUE],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location,
            ["Value"] = value
        }, buffer
    end,
    [MONITOR_CONTENDED_ENTER] = function (buffer)
        local requestID = 0
        local thread = 0
        local object = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        object, buffer = extract_objectID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[MONITOR_CONTENDED_ENTER],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Monitor Object ID"] = object,
            ["Location"] = location
        }, buffer
    end,
    [MONITOR_CONTENDED_ENTERED] = function (buffer)
        local requestID = 0
        local thread = 0
        local object = 0
        local location = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        object, buffer = extract_objectID(buffer)
        location, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[MONITOR_CONTENDED_ENTERED],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Monitor Object ID"] = object,
            ["Location"] = location
        }, buffer
    end,
    [MONITOR_WAIT] = function (buffer)
        local requestID = 0
        local thread = 0
        local object = 0
        local location = 0
        local timeout = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        object, buffer = extract_objectID(buffer)
        location, buffer  = extract_location(buffer)
        timeout, buffer = extract_long(buffer)
        
        return {
            ["Event"] = eventkinds[MONITOR_WAIT],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Monitor Object ID"] = object,
            ["Location"] = location,
            ["Timeout"] = timeout
        }, buffer
    end,
    [MONITOR_WAITED] = function (buffer)
        local requestID = 0
        local thread = 0
        local object = 0
        local location = 0
        local timed_out = false
    
        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        object, buffer = extract_objectID(buffer)
        location, buffer  = extract_location(buffer)
        timed_out, buffer = extract_boolean(buffer)
        
        return {
            ["Event"] = eventkinds[MONITOR_WAITED],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Monitor Object ID"] = object,
            ["Location"] = location,
            ["Timed out"] = timed_out
        }, buffer
    end,
    [EXCEPTION] = function(buffer)
        local requestID = 0
        local thread = 0
        local location = 0
        local exception = 0
        local catchLocation = 0
    
        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        exception, buffer = extract_tagged_objectID(buffer)
        catchLocation, buffer  = extract_location(buffer)
        
        return {
            ["Event"] = eventkinds[EXCEPTION],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Location"] = location,
            ["Exception ID"] = exception,
            ["Catch Location"] = catchLocation
        }, buffer
    end,
    [THREAD_START] = function(buffer)
        local requestID = 0
        local thread = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)

        return {
            ["Event"] = eventkinds[THREAD_START],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread
        }, buffer
    end,
    [THREAD_DEATH] = function(buffer)
        local requestID = 0
        local thread = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)

        return {
            ["Event"] = eventkinds[THREAD_DEATH],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread
        }, buffer
    end,
    [CLASS_PREPARE] = function(buffer)
        local requestID = 0
        local thread = 0
        local refTypeTag = 0
        local typeID = 0
        local signature = ""
        local status = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        refTypeTag, buffer = extract_byte(buffer)
        typeID, buffer = extract_referenceTypeID(buffer)
        signature, buffer = extract_string(buffer)
        status, buffer = extract_int(buffer)

        return {
            ["Event"] = eventkinds[CLASS_PREPARE],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Reference Type"] = typetags[refTypeTag],
            ["Type ID"] = typeID,
            ["Signature"] = signature,
            ["Status"] = classstates[status]
        }, buffer
    end,
    [CLASS_UNLOAD] = function(buffer)
        local requestID = 0
        local signature = ""

        requestID, buffer = extract_int(buffer)
        signature, buffer = extract_string(buffer)

        return {
            ["Event"] = eventkinds[CLASS_UNLOAD],
            ["Request ID"] = requestID,
            ["Signature"] = signature
        }, buffer
    end,
    [FIELD_ACCESS] = function (buffer)
        local requestID = 0
        local thread = 0
        local location = 0
        local refTypeTag = 0
        local typeID = 0
        local fieldID = 0
        local object = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        refTypeTag, buffer = extract_byte(buffer)
        typeID, buffer = extract_referenceTypeID(buffer)
        fieldID, buffer = extract_fieldID(buffer)
        object, buffer = extract_tagged_objectID(buffer)

        return {
            ["Event"] = eventkinds[FIELD_ACCESS],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Reference Type"] = typetags[refTypeTag],
            ["Type ID"] = typeID,
            ["Field ID"] = fieldID,
            ["Object ID"] = object
        }, buffer
    end,
    [FIELD_MODIFICATION] = function (buffer)
        local requestID = 0
        local thread = 0
        local location = 0
        local refTypeTag = 0
        local typeID = 0
        local fieldID = 0
        local object = 0
        local value = 0

        requestID, buffer = extract_int(buffer)
        thread, buffer = extract_threadID(buffer)
        location, buffer  = extract_location(buffer)
        refTypeTag, buffer = extract_byte(buffer)
        typeID, buffer = extract_referenceTypeID(buffer)
        fieldID, buffer = extract_fieldID(buffer)
        object, buffer = extract_tagged_objectID(buffer)
        value, buffer = extract_value(buffer)

        return {
            ["Event"] = eventkinds[FIELD_MODIFICATION],
            ["Request ID"] = requestID,
            ["Thread ID"] = thread,
            ["Reference Type"] = typetags[refTypeTag],
            ["Type ID"] = typeID,
            ["Field ID"] = fieldID,
            ["Object ID"] = object,
            ["Assigned Value"] = value
        }, buffer
    end,
    [VM_DEATH] = function (buffer)
        local requestID = 0

        requestID, buffer = extract_int(buffer)

        return {
            ["Event"] = eventkinds[VM_DEATH],
            ["Request ID"] = requestID
        }, buffer
    end
}

-- Composite Command (100)
function dissect_classobjectreference_composite(buffer)
    local suspendPolicy = 0
    local events = 0
    local eventInfo = {}

    suspendPolicy, buffer =  extract_byte(buffer)
    events, buffer  = extract_int(buffer)
    eventInfo["label"] = "Events (" .. events .. ")"
    for i=1, events do
        local eventKind = 0

        eventKind, buffer = extract_byte(buffer)
        eventInfo[i], buffer = eventKind_dispatch[eventKind](buffer)
        eventInfo[i]["label"] = "Event " .. i
    end

    return {
        ["label"] = "Composite",
        ["Suspend Policy"] = suspendpolicies[supendPolicy],
        ["Events"] = eventInfo
    }
end