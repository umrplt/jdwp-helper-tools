package.path = package.path .. ";../?.lua" 
errors = require("constants.jdwp-error")
typetags = require("constants.jdwp-typetag")
classstates = require("constants.jdwp-classstatus")
require("jdwp-sizes")
require("jdwp-fieldextractors")

-- Version Command (1)
-- No Request
-- Reply
function dissect_version_reply(buffer)
    local description = ""
    local major = 0
    local minor = 0
    local version = ""
    local name = ""

    description, buffer = extract_string(buffer)
    major, buffer = extract_int(buffer)
    minor, buffer  = extract_int(buffer)
    version, buffer = extract_string(buffer)
    name, buffer = extract_string(buffer)

    return {
        ["label"] = "Version Reply",
        ["Description"] = description,
        ["Major"] = major,
        ["Minor"] = minor,
        ["Version"] = version,
        ["Name"] = name
    }
end

-- ClassesBySignature Command (2)
-- Request
function dissect_classes_by_signature_request(buffer)
    local signature = ""

    signature, buffer = extract_string(buffer)

    return {
        ["label"] = "ClassesBySignature Request",
        ["Signature"] = signature
    }
end
-- Reply
function dissect_classes_by_signature_reply(buffer)
    local classes = 0
    local classinfo = {}

    classes, buffer = extract_int(buffer)
    for i=1, classes do
        local class = {}
        local typetagID = 0
        local typeID = 0
        local status = 0

        typetagID, buffer = extract_byte(buffer)
        typeID, buffer = extract_referenceTypeID(buffer)
        status, buffer = extract_int(buffer)

        classinfo[i] = {
            ["label"] = "Class " .. i,
            ["Reference Type"] = typetags[typetagID],
            ["Type ID"] = typeID,
            ["Status"] = classstates[status]
        }
    end

    return {
        ["label"] = "ClassesBySignature Reply",
        ["Classes"] = classinfo
    }
end

-- AllThreads Command (4)
-- No Request
-- Reply
function dissect_all_threads_reply(buffer)
    local threadcount = 0
    local threadinfo = {}

    threadcount, buffer = extract_int(buffer)
    for i=1, threadcount do
        local threadID = 0

        threadID, buffer = extract_threadID(buffer)

        threadinfo[i] = {
            ["label"] = "Thread " .. threadID
        }
    end

    return {
        ["label"] = "AllThreads Reply",
        ["Threads"] = threadinfo
    }
end

-- TopLevelThreadGroups Command (5)
-- No Request
-- Reply
function dissect_top_level_thread_groups_reply(buffer)
    local groupcount = 0
    local threadgroupinfo = {}
    
    groupcount, buffer = extract_int(buffer)
    for i=1, groupcount do
        local groupID = 0

        groupID, buffer = extract_threadGroupID(buffer)

        threadgroupinfo[i] = {
            ["label"] = "Thread Group " .. groupID
        }
    end

    return {
        ["label"] = "TopLevelThreadGroups Reply",
        ["Thread Groups"] = threadinfo
    }
end

-- Dispose Command (6)
-- No Request
-- No Reply

-- IDSizes Command (7)
-- No Request
-- Reply
function dissect_idsizes_reply(buffer)
    local sizes = JDWP_IDSIZES.get_instance()
    local fieldIDSize = 0
    local methodIDSize = 0
    local objectIDSize = 0
    local referenceTypeIDSize = 0
    local frameIDSize = 0

    fieldIDSize, buffer = extract_int(buffer)
    methodIDSize, buffer = extract_int(buffer)
    objectIDSize, buffer = extract_int(buffer)
    referenceTypeIDSize, buffer = extract_int(buffer)
    frameIDSize, buffer = extract_int(buffer)

    sizes.set_fieldID(fieldIDSize)
    sizes.set_methodID(methodIDSize)
    sizes.set_objectID(objectIDSize)
    sizes.set_referenceTypeID(referenceTypeIDSize)
    sizes.set_frameID(frameIDSize)

    return {
        ["label"] = "IDSizes Reply",
        ["FieldID Size"] = fieldIDSize, 
        ["MethodID Size"] = methodIDSize, 
        ["ObjectID Size"] = objectIDSize, 
        ["ReferenceTypeID Size"] = referenceTypeIDSize, 
        ["FrameID Size"] = frameIDSize
    }
end

-- Suspend Command (8)
-- No Request
-- No reply

-- Resume Command (9)
-- No Request
-- No reply

-- Exit Command (10)
-- Request
function dissect_exit_request(buffer)
    local exitCode = 0

    exitCode, buffer = extract_int(buffer)

    return {
        ["label"] = "Exit Request",
        ["Exit Code"] = exitCode
    }
end
-- No Reply

-- CreateString Command (11)
-- Request
function dissect_create_string_request(buffer)
    local string = ""

    string, buffer = extract_string(buffer)
    
    return {
        ["label"] = "CreateString Request",
        ["String"] = string
    }
end
-- Reply
function dissect_create_string_reply(buffer)
    local stringID = nil

    stringID, buffer = extract_stringID(buffer)

    return {
        ["label"] = "CreateString Reply",
        ["StringID"] = stringID
    }
end

-- Capabilities Command (12)
-- No Request
-- Reply
function dissect_class_paths_reply(buffer)
   local canWatchFieldModification = 0
   local canWatchFieldAccess = 0
   local canGetBytecodes = 0
   local canGetSyntheticAttribute = 0
   local canGetOwnedMonitorInfo = 0
   local canGetCurrentContendedMonitor = 0
   local canGetMonitorInfo = 0

   canWatchFieldModification, buffer = extract_boolean(buffer)
   canWatchFieldAccess, buffer = extract_boolean(buffer)
   canGetBytecodes, buffer = extract_boolean(buffer)
   canGetSyntheticAttribute, buffer = extract_boolean(buffer)
   canGetOwnedMonitorInfo, buffer = extract_boolean(buffer)
   canGetCurrentContendedMonitor, buffer = extract_boolean(buffer)
   canGetMonitorInfo, buffer = extract_boolean(buffer)

   return {
        ["label"] = "Capabilities Reply",
        ["Watch Field Modification"] = canWatchFieldModification,
        ["Watch Field Access"] = canWatchFieldAccess,
        ["Get Bytecodes"] = canGetBytecodes,
        ["Get Synthetic Attribute"] = canGetSyntheticAttribute,
        ["Get owned Monitor Info"] = canGetOwnedMonitorInfo,
        ["Get current Contended Monitor"]= canGetCurrentContendedMonitor,
        ["Get Monitor Info"] = canGetMonitorInfo
    }
end

-- ClassPaths Command (13)
-- No Request
-- Reply
function dissect_class_paths_reply(buffer)
    local basedir = ""
    local classpathcount = 0
    local classpaths = {}
    local bootclasspathcount = 0
    local bootclasspaths = {}

    basedir, buffer = extract_string(buffer)
    classpathcount, buffer = extract_int(buffer)
    for i=1, classpathcount do
        local classpath = ""

        classpath, buffer = extract_string(buffer)

        classpaths[i] = {
            ["Class Path" .. i] = classpath
        }
    end
    bootclasspathcount, buffer = extract_int(buffer)
    for i=1, bootclasspathcount do
        local bootclasspath = ""

        bootclasspath, buffer = extract_string(buffer)

        bootclasspaths[i] = {
            ["Boot Class Path" .. i] = bootclasspath
        }
    end

    return {
        ["label"] = "ClassPaths Reply",
        ["BaseDir"] = basedir,
        ["Class Paths"] = classpaths,
        ["Boot Class Paths"] = bootclasspaths
    }
end

-- DisposeObjects Command (14)
-- Request
function dissect_dispose_objects_request(buffer)
    local requestcount = 0
    local requests = {}

    requestcount, buffer = extract_int(buffer)
    for i=1, requestcount do
        local objectID = 0
        local refcount = 0

        object, buffer = extract_objectID(buffer)
        refcount, buffer = extract_int(buffer)

        requests[i] = {
            ["label"] = "Request " .. i,
            ["Object ID"] = objectID,
            ["Reference Count"] = refcount
        }
    end

    return {
        ["label"] = "DisposeObjects Request",
        ["Requests"] = requests
    }
end
-- No Reply

-- HoldEvents Command (15)
-- No Request
-- No Reply

-- ReleaseEvents Command (16)
-- No Request
-- No Reply

-- CapabilitiesNew Command (17)
-- No Request
-- Reply
function dissect_capabilities_new_reply(buffer)
    local canWatchFieldModification = 0
    local canWatchFieldAccess = 0
    local canGetBytecodes = 0
    local canGetSyntheticAttribute = 0
    local canGetOwnedMonitorInfo = 0
    local canGetCurrentContendedMonitor = 0
    local canGetMonitorInfo = 0
    local canRedefineClasses = 0
    local canAddMethod = 0
    local canUnrestrictedlyRedefineClasses = 0
    local canPopFrames = 0
    local canUseInstanceFilters = 0
    local canGetSourceDebugExtension = 0
    local canRequestVMDeathEvent = 0
    local canSetDefaultStratum = 0
    local canGetInstanceInfo = 0
    local canRequestMonitorEvents = 0
    local canGetMonitorFrameInfo = 0
    local canUseSourceNameFilters = 0
    local canGetConstantPool = 0
    local canForceEarlyReturn = 0
 
    canWatchFieldModification, buffer = extract_boolean(buffer)
    canWatchFieldAccess, buffer = extract_boolean(buffer)
    canGetBytecodes, buffer = extract_boolean(buffer)
    canGetSyntheticAttribute, buffer = extract_boolean(buffer)
    canGetOwnedMonitorInfo, buffer = extract_boolean(buffer)
    canGetCurrentContendedMonitor, buffer = extract_boolean(buffer)
    canGetMonitorInfo, buffer = extract_boolean(buffer)
    canRedefineClasses, buffer = extract_boolean(buffer)
    canAddMethod, buffer = extract_boolean(buffer)
    canUnrestrictedlyRedefineClasses, buffer = extract_boolean(buffer)
    canPopFrames, buffer = extract_boolean(buffer)
    canUseInstanceFilters, buffer = extract_boolean(buffer)
    canGetSourceDebugExtension, buffer = extract_boolean(buffer)
    canRequestVMDeathEvent, buffer = extract_boolean(buffer)
    canSetDefaultStratum, buffer = extract_boolean(buffer)
    canGetInstanceInfo, buffer = extract_boolean(buffer)
    canRequestMonitorEvents, buffer = extract_boolean(buffer)
    canGetMonitorFrameInfo, buffer = extract_boolean(buffer)
    canUseSourceNameFilters, buffer = extract_boolean(buffer)
    canGetConstantPool, buffer = extract_boolean(buffer)
    canForceEarlyReturn, buffer = extract_boolean(buffer)
 
    return {
         ["label"] = "Capabilities Reply",
         ["Watch Field Modification"] = canWatchFieldModification,
         ["Watch Field Access"] = canWatchFieldAccess,
         ["Get Bytecodes"] = canGetBytecodes,
         ["Get Synthetic Attribute"] = canGetSyntheticAttribute,
         ["Get owned Monitor Info"] = canGetOwnedMonitorInfo,
         ["Get current Contended Monitor"]= canGetCurrentContendedMonitor,
         ["Get Monitor Info"] = canGetMonitorInfo,
         ["Redefine Classes"] = canRedefineClasses,
         ["Add Method"] = canAddMethod,
         ["Unrestrictedly Redefine Classes"] = canUnrestrictedlyRedefineClasses,
         ["Pop Frames"] = canPopFrames,
         ["Use Instance Filters"] = canUseInstanceFilters,
         ["Get Source Debug Extension"] = canGetSourceDebugExtension,
         ["Request VM Death Event"] = canRequestVMDeathEvent,
         ["Set Default Stratum"] = canSetDefaultStratum,
         ["Get Instance Info"] = canGetInstanceInfo,
         ["Request Monitor Events"] = canRequestMonitorEvents,
         ["Get Monitor Frame Info"] = canGetMonitorFrameInfo,
         ["Use Source Name Filters"] = canUseSourceNameFilters,
         ["Get Constant Pool"] = canGetConstantPool,
         ["Force Early Return"] = canForceEarlyReturn
     }
end

-- RedefineClasses Command (18)
-- Request
function dissect_redefine_classes_request(buffer)
    local classcount = 0
    local classes = {}

    classcount, buffer = extract_int(buffer)
    for i=1, classcount do
        local refTypeID = 0
        local classfilesize = 0

        refTypeID, buffer = extract_referenceTypeID(buffer)
        classfilesize, buffer = extract_int(buffer)

        classes[i] = {
            ["label"] = "Redefined Class " .. i,
            ["Reference Type ID"] = refTypeID,
            ["Size"] = classfilesize
        }
    end

    return {
        ["label"] = "RedefineClasses Reply",
        ["Redefined Classes"] = classes
    }
end
-- No Reply

-- SetDefaultStratum Command (19)
-- Request
function dissect_set_default_statum_request(buffer)
    local stratumID = ""

    stratumID, buffer = extract_string(buffer)

    return {
        ["label"] = "SetDefaultStratum Request",
        ["Stratum ID"] = stratumID
    }
end
-- No Reply

-- AllClassesWithGeneric Command (20)
-- No Request
-- Reply
function dissect_all_classes_with_generic_reply(buffer)
    local classcount = 0
    local classes = {}

    classcount, buffer = extract_int(buffer)
    for i=1, classcount do
        local typetagID = 0
        local referenceTypeID = 0
        local signature = ""
        local genericsignature = ""
        local status = 0

        typetagID, buffer = extract_byte(buffer)
        referenceTypeID, buffer = extract_referenceTypeID(buffer)
        signature, buffer = extract_string(buffer)
        genericsignature, buffer = extract_string(buffer)
        status, buffer = extract_int(buffer)

        classes[i] = {
            ["label"] = "Class " .. i,
            ["Reference Type"] = typetags[typetagID],
            ["Signature"] = signature,
            ["Generic Signature"] = genericsignature
        }
    end

    return {
        ["label"] = "RedefineClasses Reply",
        ["Classes"] = classes
    }
end

-- InstanceCounts Command (21)
-- Request
function dissect_instance_counts_request(buffer)
    local reftypescount = 0
    local reftypes = {}

    reftypescount, buffer = extract_referenceTypeID(buffer)
    for i=1, reftypescount do
        local reftypeid = 0

        reftypeid, buffer = extract_referenceTypeID(buffer)

        reftypes[i] = {
            ["label"] = "ReferenceTypeID " .. reftypeid
        }
    end

    return {
        ["label"] = "InstanceCounts Request",
        ["Classes"] = reftypes
    }
end
-- Reply
function dissect_instance_counts_reply(buffer)
    local counts = 0
    local instancecounts = {}

    counts, buffer = extract_int(buffer)
    for i=1, counts do
        local count = 0

        count, buffer = extract_long(buffer)

        instancecounts[i] = {
            ["label"] = "ReferenceTypeID " .. i,
            ["Count"] = count
        }
    end

    return {
        ["label"] = "InstanceCounts Reply",
        ["Instance Counts"] = instancecounts
    }
end