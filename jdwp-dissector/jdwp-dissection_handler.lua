DissectionHandler = {}
DissectionHandler.__index = DissectionHandler

setmetatable(DissectionHandler, {
    __call = function (cls, ...)
      return cls.new(...)
    end,
})

function DissectionHandler.new(req, rep)
    local self = setmetatable({}, DissectionHandler)
    self.request = req
    self.reply = rep
    return self
end

function DissectionHandler:getRequest()
    return self.request
end

function DissectionHandler:getReply()
    return self.reply
end