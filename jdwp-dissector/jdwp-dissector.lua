JDWP_PORT = 5005 -- TODO: Auto-detect port
JDWP_HANDSHAKE_SIZE = 14 -- "JDWP-HANDSHAKE"
JDWP_HEADER_SIZE = 11
JDWP_REPLY_FLAG = 0x80

local BinaryFormat = package.cpath:match("%p[\\|/]?%p(%a+)")
separator = ""
if BinaryFormat == "dll" then
    function os.name()
        return "Windows"
    end
elseif BinaryFormat == "so" then
    function os.name()
        return "Linux"
    end
elseif BinaryFormat == "dylib" then
    separator = "/"
    function os.name()
        return "MacOS"
    end
end

require("jdwp-dissection_handler")
require("jdwp-sizes")
-- Require constants
commandsets = require("constants.jdwp-commandsets")
errors = require("constants.jdwp-error")
eventkinds = require("constants.jdwp-eventkind")
threadstates = require("constants.jdwp-threadstatus")
suspendstates = require("constants.jdwp-suspendstatus")
classstates = require("constants.jdwp-classstatus")
typetags = require("constants.jdwp-typetag")
tags = require("constants.jdwp-tag")
stepdepths = require("constants.jdwp-stepdepth")
stepsizes = require("constants.jdwp-stepsize")
supendpolicies = require("constants.jdwp-suspendpolicy")
invokeoptions = require("constants.jdwp-invokeoptions")
--Require command dissectors
require("commands.virtualmachine")
require("commands.event")
require("commands.referencetype")
require("commands.classtype")
require("commands.arraytype")
require("commands.interfacetype")
require("commands.method")
require("commands.field")
require("commands.objectreference")

require("commands.threadreference")

require("commands.eventrequest")

jdwp_proto = Proto("jdwp", "Java Debug Wire Protocol")
jdwp_proto.fields.message = ProtoField.new("Message", "jdwp.message", ftypes.STRING)
jdwp_proto.fields.id = ProtoField.new("ID", "jdwp.id", ftypes.INT32)
jdwp_proto.fields.flags = ProtoField.new("Flags", "jdwp.flags", ftypes.UINT8)
jdwp_proto.fields.commandset = ProtoField.new("Command Set", "jdwp.commandset", ftypes.UINT8)
jdwp_proto.fields.command = ProtoField.new("Command", "jdwp.command", ftypes.UINT8)
jdwp_proto.fields.errorcode = ProtoField.new("Error Code", "jdwp.error", ftypes.UINT16)

expected_replies = {}
dissectors = {
    [VM_COMMANDSET] = {
        [VM_VERSION_COMMAND] = DissectionHandler.new(nil, dissect_version_reply),
        [VM_CLASSES_BY_SIGNATURE_COMMAND] = DissectionHandler.new(dissect_classes_by_signature_request, dissect_classes_by_signature_reply),
        [VM_ALL_CLASSES_COMMAND] = DissectionHandler.new(nil, dissect_all_classes_reply),
        [VM_ALL_THREADS_COMMAND] = DissectionHandler.new(nil, dissect_all_threads_reply),
        [VM_TOP_LEVEL_THREAD_GROUPS_COMMAND] = DissectionHandler:new(nil, dissect_top_level_thread_groups_reply),
        [VM_DISPOSE_COMMAND] = DissectionHandler.new(nil, nil),
        [VM_IDSIZES_COMMAND] = DissectionHandler.new(nil, dissect_idsizes_reply),
        [VM_SUSPEND_COMMAND] = DissectionHandler.new(nil, nil),
        [VM_RESUME_COMMAND] = DissectionHandler.new(nil, nil),
        [VM_EXIT_COMMAND] = DissectionHandler.new(dissect_exit_request, nil),
        [VM_CREATE_STRING_COMMAND] = DissectionHandler.new(dissect_create_string_request, dissect_create_string_reply),
        [VM_CAPABILITIES_COMMAND] = DissectionHandler.new(nil, dissect_capabiliteis_reply),
        [VM_CLASSPATHS_COMMAND] = DissectionHandler.new(nil, dissect_class_paths_reply),
        [VM_DISPOSE_OBJECTS_COMMAND] = DissectionHandler:new(dissect_dispose_objects_request, nil),
        [VM_HOLD_EVENTS_COMMAND] = DissectionHandler.new(nil, nil),
        [VM_RELEASE_EVENTS_COMMAND] = DissectionHandler.new(nil, nil),
        [VM_CAPABILITIES_NEW_COMMAND] = DissectionHandler.new(nil, dissect_capabilities_new_reply),
        [VM_REDEFINE_CLASSES_COMMAND] = DissectionHandler.new(dissect_redefine_classes_request, nil),
        [VM_SET_DEFAULT_STRATUM_COMMAND] = DissectionHandler.new(dissect_set_default_statum_request, nil),
        [VM_ALL_CLASSES_WITH_GENERIC_COMMAND] = DissectionHandler.new(nil, dissect_all_classes_with_generic_reply),
        [VM_INSTANCE_COUNTS_COMMAND] = DissectionHandler.new(dissect_instance_counts_request, dissect_instance_counts_reply)
    },
    [REFERENCE_TYPE_COMMANDSET] = {
        [REFERENCE_TYPE_SIGNATURE_COMMAND] = DissectionHandler.new(dissect_signature_request, dissect_signature_reply),
        [REFERENCE_TYPE_CLASSLOADER_COMMAND] = DissectionHandler.new(dissect_class_loader_request, dissect_class_loader_reply),
        [REFERENCE_TYPE_MODIFIERS_COMMAND] = DissectionHandler.new(dissect_modifiers_request, dissect_modifiers_reply),  
        [REFERENCE_TYPE_FIELDS_COMMAND] = DissectionHandler.new(dissect_fields_request, dissect_fields_reply),
        [REFERENCE_TYPE_METHODS_COMMAND] = DissectionHandler.new(dissect_methods_request, dissect_methods_reply),
        [REFERENCE_TYPE_GET_VALUES_COMMAND] = DissectionHandler.new(dissect_get_values_request, dissect_get_values_reply),
        [REFERENCE_TYPE_SOURCE_FILE_COMMAND] = DissectionHandler.new(dissect_source_file_request, dissect_source_file_reply),
        [REFERENCE_TYPE_NESTED_TYPES_COMMAND] = DissectionHandler.new(dissect_nested_types_request, dissect_nested_types_reply),
        [REFERENCE_TYPE_STATUS_COMMAND] = DissectionHandler.new(dissect_referencetypestatus_request, dissect_referencetypestatus_reply),
        [REFERENCE_TYPE_INTERFACES_COMMAND] = DissectionHandler.new(dissect_interfaces_request, dissect_interfaces_reply),
        [REFERENCE_TYPE_CLASS_OBJECT_COMMAND] = DissectionHandler.new(dissect_class_object_request, dissect_class_object_reply),
        [REFERENCE_TYPE_SOURCE_DEBUG_EXTENSION_COMMAND] = DissectionHandler.new(dissect_source_debug_extension_request, dissect_source_debug_extension_reply),
        [REFERENCE_TYPE_SIGNATURE_WITH_GENERIC_COMMAND] = DissectionHandler.new(dissect_signature_with_generic_request, dissect_signature_with_generic_reply),
        [REFERENCE_TYPE_FIELDS_WITH_GENERIC_COMMAND] = DissectionHandler.new(dissect_fields_with_generic_request, dissect_fields_with_generic_reply),
        [REFERENCE_TYPE_METHODS_WITH_GENERIC_COMMAND] = DissectionHandler.new(dissect_methods_with_generic_request, dissect_methods_with_generic_reply),
        [REFERENCE_TYPE_INSTANCES_COMMAND] = DissectionHandler.new(dissect_instances_request, dissect_instances_reply),
        [REFERENCE_TYPE_CLASS_FILE_VERSION_COMMAND] = DissectionHandler.new(dissect_classfile_version_request, dissect_classfile_version_reply),
        [REFERENCE_TYPE_CONSTANT_POOL_COMMAND] = DissectionHandler.new(dissect_constant_pool_request, dissect_cosntant_pool_reply)
    },
    [CLASS_TYPE_COMMANDSET] = {
        [CLASS_TYPE_SUPERCLASS_COMMAND] = DissectionHandler.new(dissect_superclass_request, dissect_superclass_reply),
        [CLASS_TYPE_SETVALUES_COMMAND] = DissectionHandler.new(dissect_setvalues_request, nil),
        [CLASS_TYPE_INVOKEMETHOD_COMMAND] = DissectionHandler.new(dissect_invokedynamicmethod_request, dissect_invokedynamicmethod_reply),
        [CLASS_TYPE_NEWINSTANCE_COMMAND] = DissectionHandler.new(dissect_newclassinstance_request, dissect_newclassinstance_reply)
    },
    [ARRAY_TYPE_COMMANDSET] = {
        [ARRAY_TYPE_NEWINSTANCE_COMMAND] = DissectionHandler.new(dissect_newarrayinstance_request, dissect_newarrayinstance_reply)
    },
    [INTERFACE_TYPE_COMMANDSET] = {
        [INTERFACE_TYPE_INVOKEMETHOD_COMMAND] = DissectionHandler.new(dissect_invokeinterfacemethod_request, dissect_invokeinterfacemethod_reply)
    },
    [METHOD_COMMANDSET] = {
        [METHOD_LINETABLE_COMMAND] = DissectionHandler.new(dissect_linetable_request, dissect_linetable_reply),
        [METHOD_VARIABLETABLE_COMMAND] = DissectionHandler.new(dissect_variabletable_request, dissect_variabletable_reply),
        [METHOD_BYTECODES_COMMAND] = DissectionHandler.new(dissect_bytecodes_request, dissect_bytecodes_reply),
        [METHOD_ISOBSOLETE_COMMAND] = DissectionHandler.new(dissect_isobsolete_request, dissect_isobsolete_reply),
        [METHOD_VARIABLETABLEWITHGENERIC_COMMAND] = DissectionHandler.new(dissect_variabletablewithgeneric_request, dissect_variabletablewithgeneric_reply)
    },
    [FIELD_COMMANDSET] = {},
    [OBJECT_REFERENCE_COMMANDSET] = {
        [OBJECT_REFERENCE_REFERENCETYPE] = DissectionHandler.new(dissect_referencetype_request, dissect_referencetype_reply),
        [OBJECT_REFERENCE_GETVALUES] = DissectionHandler.new(dissect_getvalues_request, dissect_getvalues_reply),
        [OBJECT_REFERENCE_SETVALUES] = DissectionHandler.new(dissect_setvalues_request, nil),
        [OBJECT_REFERENCE_MONITORINFO] = DissectionHandler.new(dissect_monitorinfo_request, dissect_monitorinfo_reply),
        [OBJECT_REFERENCE_INVOKEMETHOD] = DissectionHandler.new(dissect_invokeinstancemethod_request, dissect_invokeinstancemethod_reply),
        [OBJECT_REFERENCE_DISABLECOLLECTION] = DissectionHandler.new(dissect_disablecollection_request, nil),
        [OBJECT_REFERENCE_ENABLECOLLECTION] = DissectionHandler.new(dissect_enablecollection_request, nil),
        [OBJECT_REFERENCE_ISCOLLECTED] = DissectionHandler.new(dissect_iscollected_request, dissect_iscollected_reply),
        [OBJECT_REFERENCE_REFERRINGOBJECTS] = DissectionHandler.new(dissect_referringobjects_request, dissect_referringobjects_reply)
    },
    [OBJECT_REFERENCE_COMMANDSET] = {
        [OBJECT_REFERENCE_REFERENCETYPE] = DissectionHandler.new(dissect_referencetype_request, dissect_referencetype_reply),
        [OBJECT_REFERENCE_GETVALUES] = DissectionHandler.new(dissect_getobjectvalues_request, dissect_getobjectvalues_reply),
        [OBJECT_REFERENCE_SETVALUES] = DissectionHandler.new(dissect_setobjectvalues_request, nil),
        [OBJECT_REFERENCE_MONITORINFO] = DissectionHandler.new(dissect_objectmonitorinfo_request, dissect_objectmonitorinfo_reply),
        [OBJECT_REFERENCE_INVOKEMETHOD] = DissectionHandler.new(dissect_invokeobjectmethod_request, dissect_invokeobjectmethod_reply),
        [OBJECT_REFERENCE_DISABLECOLLECTION] = DissectionHandler.new(dissect_disableobjectcollection_request, nil),
        [OBJECT_REFERENCE_ENABLECOLLECTION] = DissectionHandler.new(dissect_enableobjectcollection_request, nil),
        [OBJECT_REFERENCE_ISCOLLECTED] = DissectionHandler.new(dissect_isobjectcollected_request, dissect_isobjectcollected_reply),
        [OBJECT_REFERENCE_REFERRINGOBJECTS] = DissectionHandler.new(dissect_objectreferringobjects_request, dissect_objectreferringobjects_reply)
    },
    [OBJECT_REFERENCE_COMMANDSET] = {
        [OBJECT_REFERENCE_REFERENCETYPE] = DissectionHandler.new(dissect_referencetype_request, dissect_referencetype_reply),
        [OBJECT_REFERENCE_GETVALUES] = DissectionHandler.new(dissect_getobjectvalues_request, dissect_getobjectvalues_reply),
        [OBJECT_REFERENCE_SETVALUES] = DissectionHandler.new(dissect_setobjectvalues_request, nil),
        [OBJECT_REFERENCE_MONITORINFO] = DissectionHandler.new(dissect_objectmonitorinfo_request, dissect_objectmonitorinfo_reply),
        [OBJECT_REFERENCE_INVOKEMETHOD] = DissectionHandler.new(dissect_invokeobjectmethod_request, dissect_invokeobjectmethod_reply),
        [OBJECT_REFERENCE_DISABLECOLLECTION] = DissectionHandler.new(dissect_disableobjectcollection_request, nil),
        [OBJECT_REFERENCE_ENABLECOLLECTION] = DissectionHandler.new(dissect_enableobjectcollection_request, nil),
        [OBJECT_REFERENCE_ISCOLLECTED] = DissectionHandler.new(dissect_isobjectcollected_request, dissect_isobjectcollected_reply),
        [OBJECT_REFERENCE_REFERRINGOBJECTS] = DissectionHandler.new(dissect_objectreferringobjects_request, dissect_objectreferringobjects_reply)
    },
    [STRING_REFERENCE_COMMANDSET] = {
        [STRING_REFERENCE_VALUE] = DissectionHandler.new(dissect_value_request, dissect_value_reply)
    },
    [THREAD_REFERENCE_COMMANDSET] = {
        [THREAD_REFERENCE_NAME] = DissectionHandler.new(dissect_name_request, dissect_name_reply),
        [THREAD_REFERENCE_SUSPEND] = DissectionHandler.new(dissect_suspend_request, nil),
        [THREAD_REFERENCE_RESUME] = DissectionHandler.new(dissect_resume_request, nil),
        [THREAD_REFERENCE_STATUS] = DissectionHandler.new(dissect_threadstatus_request, dissect_threadstatus_reply),
        [THREAD_REFERENCE_THREADGROUP] = DissectionHandler.new(dissect_threadgroup_request, dissect_threadgroup_reply),
        [THREAD_REFERENCE_FRAMES] = DissectionHandler.new(dissect_frames_request, dissect_frames_reply),
        [THREAD_REFERENCE_FRAMECOUNT] = DissectionHandler.new(dissect_framecount_request, dissect_framecount_reply),
        [THREAD_REFERENCE_OWNEDMONITORS] = DissectionHandler.new(dissect_ownedmonitors_request, dissect_ownedmonitors_reply),
        [THREAD_REFERENCE_CURRENTCONTENDEDMONITOR] = DissectionHandler.new(dissect_currentcontendedmonitor_request, dissect_currentcontendedmonitor_reply),
        [THREAD_REFERENCE_STOP] = DissectionHandler.new(dissect_stop_request, nil),
        [THREAD_REFERENCE_INTERRUPT] = DissectionHandler.new(dissect_interrupt_request, nil),
        [THREAD_REFERENCE_SUSPENDCOUNT] = DissectionHandler.new(dissect_suspendcount_request, dissect_suspendcount_reply),
        [THREAD_REFERENCE_OWNEDMONITORSSTACKDEPTHINFO] = DissectionHandler.new(dissect_ownedmonitorsstackdepthinfo_request, dissect_ownedmonitorsstackdepthinfo_reply),
        [THREAD_REFERENCE_FORCEEARLYRETURN] = DissectionHandler.new(dissect_forceearlyreturn_request, nil)
    },
    [THREADGROUP_REFERENCE_COMMANDSET] = {
        [THREADGROUP_REFERENCE_NAME] = DissectionHandler.new(dissect_threadgroup_name_request, dissect_threadgroup_name_reply),
        [THREADGROUP_REFERENCE_PARENT] = DissectionHandler.new(dissect_parent_request, dissect_parent_request),
        [THREADGROUP_REFERENCE_CHILDREN] = DissectionHandler.new(dissect_children_request, dissect_children_reply)
    },
    [ARRAY_REFERENCE_COMMANDSET] = {
        [ARRAY_REFERENCE_LENGTH] = DissectionHandler.new(dissect_length_request, dissect_length_reply),
        [ARRAY_REFERENCE_GETVALUES] = DissectionHandler.new(dissect_getvalues_request, dissect_getvalues_reply),
        [ARRAY_REFERENCE_SETVALUES] = DissectionHandler.new(dissect_setvalues_request, nil)
    },
    [CLASSLOADER_REFERENCE_COMMANDSET] = {
        [CLASSLOADER_REFERENCE_VISIBLECLASSES] = DissectionHandler.new(dissect_visibleclasses_request, dissect_visibleclasses_reply)
    },
    [EVENTREQUEST_COMMANDSET] = {
        [EVENTREQUEST_SET] = DissectionHandler.new(dissect_set_request, dissect_set_reply),
        [EVENTREQUEST_CLEAR] = DissectionHandler.new(dissect_clear_request, nil),
        [EVENTREQUEST_CLEARALLBREAKPOINTS] = DissectionHandler.new(nil, nil)
    },
    [STACKFRAME_COMMANDSET] = {
        STACKFRAME_GETVALUES = DissectionHandler.new(dissect_stackframe_getvalues_request, dissect_stackframe_getvalues_reply),
        STACKFRAME_SETVALUES = DissectionHandler.new(dissect_stackframe_setvalues_request, nil),
        STACKFRAME_THISOBJECT = DissectionHandler.new(dissect_stackframe_thisobject_request, dissect_stackframe_thisobject_request),
        STACKFRAME_POPFRAMES = DissectionHandler.new(dissect_stackframe_popframes_request, nil)
    },
    [CLASSOBJECT_REFERENCE_COMMANDSET] = {
        [CLASSOBJECT_REFERENCE_REFLECTEDTYPE] = DissectionHandler.new(dissect_reflectedtype_request, dissect_reflectedtype_reply)
    },
    [EVENT_COMMANDSET] = {
        [CLASSOBJECTREFERENCE_COMPOSITE] = DissectionHandler.new(dissect_classobjectreference_composite, nil)
    }
}

function jdwp_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "JDWP"
    local subtree = tree:add(jdwp_proto, buffer(), "Java Debug Wire Protocol Data")
    dissect_jdwp_message(buffer, pinfo, subtree)
end

function dissect_jdwp_message(buffer, pinfo, tree)
    if buffer:len() == JDWP_HANDSHAKE_SIZE then
        tree:add(jdwp_proto.fields.message, buffer(0, JDWP_HANDSHAKE_BYTES))
    else
        offset = 0
        length = buffer:range(offset, 4):uint()
        offset = offset + 4

        id = buffer:range(offset, 4):uint()
        tree:add(jdwp_proto.fields.id, buffer(offset, 4))
        offset = offset + 4

        flags = buffer:range(offset, 1):uint()
        tree:add(jdwp_proto.fields.flags, buffer(offset, 1))
        offset = offset + 1

        if not is_reply(flags) then
            subtree = tree:add(jdwp_proto, buffer(offset, 2), "Command: " .. commandsets[buffer(offset, 1):uint()]["setname"] .. " - " .. commandsets[buffer(offset, 1):uint()][buffer(offset+1, 1):uint()])
            
            commandset = buffer:range(offset, 1):uint()
            subtree:add(jdwp_proto.fields.commandset, buffer(offset, 1))
            offset = offset + 1
            
            command = buffer:range(offset, 1):uint()
            subtree:add(jdwp_proto.fields.command, buffer(offset, 1))
            offset = offset + 1
            
            dissector = dissectors[commandset][command]
            
            if dissector:getRequest() ~= nil then
                contents = dissector:getRequest()(buffer(offset), pinfo)
                display_contents(tree, buffer(offset), contents)
            end
            
            if dissector:getReply() ~= nil then
                expected_replies[id] = dissector:getReply()
            end

           -- pinfo.cols.type = "Request"
           -- pinfo.cols.commandset = commandsets[buffer(offset, 1):uint()]["setname"]
           -- pinfo.cols.command = commandsets[buffer(offset, 1):uint()][buffer(offset+1, 1):uint()]
        else
            error = buffer:range(offset, 2):uint()
            if error ~= 0 then
                tree:add(jdwp_proto.fields.errorcode, buffer(offset, 2))
            end
            offset = offset + 2
            if expected_replies[id] ~= nil then
                contents = expected_replies[id](buffer(offset))
                expected_replies[id] = nil
                display_contents(tree, buffer(offset), contents)
            end

          --  pinfo.cols.type = "Reply"
        end
    end
end

function display_contents(tree, buffer, contents, label)
    if buffer:len() == 0 then
        return
    end

    local subtree = tree:add(jdwp_proto, buffer(), label or contents["label"] or "Contents")
    for key, value in pairs(contents) do
        if type(value) == "table" then
            display_contents(subtree, buffer, value)
        elseif key ~= "label" then
            subtree:add(jdwp_proto, buffer(), key .. ": " .. tostring(value))
        end
    end
end

function is_reply(flag)
    return flag == JDWP_REPLY_FLAG
end

tcp_table = DissectorTable.get("tcp.port")
tcp_table:add(JDWP_PORT,jdwp_proto)