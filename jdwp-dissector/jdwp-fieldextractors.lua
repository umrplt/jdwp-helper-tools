require("jdwp-sizes")
require("constants.jdwp-tag")

local function buffer_consumed(buffer, datasize)
    return buffer:len() == datasize
end

function extract_field(buffer, typestring, converter)
    local sizes = JDWP_IDSIZES.get_instance()
    local valuesize = sizes[typestring]
    local localbuffer = buffer
    local value = nil

    value = localbuffer(0, valuesize)
    if converter then
        value = converter(value)
    end

    if not buffer_consumed(localbuffer, valuesize) then
        localbuffer = localbuffer(valuesize)
    end

    return value, localbuffer
end

function extract_variable(buffer, typestring)
    local sizes = JDWP_IDSIZES.get_instance()
    local valuesize = sizes[typestring]
    local localbuffer = buffer

    local value = localbuffer(0, valuesize)
end

function extract_byte(buffer)
    return extract_field(buffer, "byte", TvbRange.uint)
end

function extract_boolean(buffer)
    local bool = false
    bool, buffer = extract_field(buffer, "boolean", TvbRange.uint)
    return bool ~= 0, buffer
end

function extract_int(buffer)
    return extract_field(buffer, "int", TvbRange.uint)
end

function extract_long(buffer)
    return extract_field(buffer, "long", TvbRange.int64)
end

function extract_char(buffer)
    return extract_field(buffer, "char", TvbRange.string)
end

function extract_char(buffer)
    return extract_field(buffer, "float", TvbRange.float)
end

function extract_char(buffer)
    return extract_field(buffer, "double", TvbRange.float)
end

function extract_referenceTypeID(buffer)
    return extract_field(buffer, "referenceTypeID")
end

function extract_arrayTypeID(buffer)
    return extract_field(buffer, "arrayTypeID")
end

function extract_objectID(buffer)
    return extract_field(buffer, "objectID")
end

function extract_tagged_objectID(buffer)
    return extract_field(buffer, "tagged-objectID")
end

function extract_stringID(buffer)
    return extract_field(buffer, "stringID")
end

function extract_threadID(buffer)
    return extract_field(buffer, "threadID")
end

function extract_threadGroupID(buffer)
    return extract_field(buffer, "threadGroupID")
end

function extract_classLoaderID(buffer)
    return extract_field(buffer, "classLoaderID")
end

function extract_classObjectID(buffer)
    return extract_field(buffer, "classObjectID")
end

function extract_fieldID(buffer)
    return extract_field(buffer, "fieldID")
end

function extract_classID(buffer)
    return extract_field(buffer, "classID")
end

function extract_interfaceID(buffer)
    return extract_field(buffer, "interfaceID")
end

function extract_frameID(buffer)
    return extract_field(buffer, "frameID")
end

function extract_classObjectID(buffer)
    return extract_field(buffer, "classObjectID")
end

function extract_location(buffer)
    return extract_field(buffer, "location")
end

function extract_string(buffer)
    local offset = 0
    local localbuffer = buffer
    local length = 0
    local value = ""
    
    length, localbuffer = extract_int(localbuffer)
    if length > localbuffer:len() then
        return nil, localbuffer
    else
        value = localbuffer(0, length):string()
        
        if not buffer_consumed(localbuffer, length) then
            localbuffer = localbuffer(length)
        end
        
        return value, localbuffer
    end
end

function extract_value(buffer)
    local tags_dispatch = {
        [ARRAY] = extract_objectID,
        [BYTE] = extract_byte,
        [CHAR] = extract_char,
        [OBJECT] = extract_objectID,
        [FLOAT] = extract_float,
        [DOUBLE] = extract_double,
        [INT] = extract_int,
        [LONG] = extract_long,
        [SHORT] = extract_short,
        [VOID] = nil,
        [BOOLEAN] = extract_boolean,
        [STRING] = extract_objectID,
        [THREAD] = extract_objectID,
        [THREAD_GROUP] = extract_objectID,
        [CLASS_LOADER] = extract_objectID,
        [CLASS_OBJECT] = extract_objectID
    }

    local localbuffer = buffer
    local signature = 0
    local value = 0

    signature, localbuffer = extract_byte(localbuffer)
    value, localbuffer = tags_dispatch[signature](localbuffer)

    return value, localbuffer
end

function extract_untagged_value(buffer)
    local localbuffer = buffer
    local signature = 0
    local value = 0

    --signature, localbuffer = extract_byte(localbuffer)
    --value, localbuffer = tags_dispatch[signature](localbuffer)

    return value, localbuffer
end