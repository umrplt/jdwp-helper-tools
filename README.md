
### Installing the wireshark plugin
Drop the "jdwp-dissector" folder into wireshark's plugin folder.
See https://www.wireshark.org/docs/wsug_html_chunked/ChPluginFolders.html if you need help locating the plugin folder.
The plugin checks **port 5005** for JDWP messages.

### How to analyze JDWP communication streams using a real application
1) Run wireshark on the loopback device with the "jdwp" filter

2) Run "java -agentlib:jdwp=transport=dt_socket,server=y,address=5005,suspend=y Main" to start the application and suspend the VM until a debugger connects

3) In Eclipse use a "Remote Java Application" run configuration with the connection properties 
        Host: localhost
        Port: 5005
    To connect to the VM.
    
4) Use wireshark to inspect the packages

### Recording a JDWP communication stream
1) Run server.py

2) Run repeater.py

3) Run the same Eclipse configuration used in the previous section

4) The repeater script will print the data sections of the JDWP messages to the console.


By default, only the server's responses are printed.
You can change this by changing the last arguments in lines 28 and 29 of repeater.py. 

### Info on JDWP
See the following links for details:
- https://docs.oracle.com/javase/10/docs/specs/jdwp/jdwp-spec.html
- https://docs.oracle.com/javase/10/docs/specs/jdwp/jdwp-protocol.html

### Additional Links
- Sedona Homepage: https://sedona-alliance.org/index.htm

- Sedona Doc: https://sedona-alliance.org/archive/doc/index.html

- Amir's implementation: https://github.com/ztimekeeper/SedonaVM_Debug

- Fork of original SedonaVM repo: https://github.com/linsong/sedona